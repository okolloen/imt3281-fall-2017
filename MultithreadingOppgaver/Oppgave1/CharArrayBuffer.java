import java.util.Vector;

public class CharArrayBuffer {
	Vector<Character> buffer = new Vector<Character>();
	
	public CharArrayBuffer() {
		buffer.add('H');
		buffer.add('e');
		buffer.add('l');
		buffer.add('l');
		buffer.add('o');
		buffer.add(' ');
		buffer.add('W');
		buffer.add('o');
		buffer.add('r');
		buffer.add('l');
		buffer.add('d');
		buffer.add('!');
	}
	
	public boolean hasMore() {
		return buffer.size()>0;
	}

	public char next() {
		return buffer.remove(0);
	}
}
