import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultithreadedHelloWorld {	
	
	public static void main(String[] args) {
		CharArrayBuffer buffer = new CharArrayBuffer();

		Writer w1 = new Writer(buffer, 1);
		Writer w2 = new Writer(buffer, 2);
		Writer w3 = new Writer(buffer, 3);
		
		ExecutorService executorService = Executors.newCachedThreadPool();
		executorService.execute(w1);
		executorService.execute(w2);
		executorService.execute(w3);
		executorService.shutdown();
	}
}
