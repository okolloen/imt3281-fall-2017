
public class Writer implements Runnable {
	CharArrayBuffer buffer = null;
	private int threadNumber;
	
	public Writer(CharArrayBuffer buffer, int threadNumber) {
		this.threadNumber = threadNumber;
		this.buffer = buffer;
	}
	
	public void run() {
		while (buffer.hasMore()) {
			char letter = buffer.next();
			System.out.println(threadNumber+": "+letter);
			try {
				Thread.sleep(3);
			} catch (InterruptedException ie) {
				
			}
		}
	}
}
