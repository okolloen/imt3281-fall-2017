// Fig. 28.2: ReadServerFileTest.java
// Create and start a ReadServerFile.
import javax.swing.JFrame;

public class ReadServerFileTest {

    // http://www.deitel.com/test/test.html
    public static void main(String[] args) {
        ReadServerFile application = new ReadServerFile();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}