import java.awt.BorderLayout;
import java.awt.Font;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class DatagramClient extends JFrame {
    private JTextField enterField;
    private JTextArea displayArea;
    private DatagramSocket socket;

    public DatagramClient() {
        super("Client");

        enterField = new JTextField("Type message here");
        enterField.setFont(new Font("Arial", Font.PLAIN, 26));
        enterField
                .addActionListener((e) -> {
                    try {
                        String message = e.getActionCommand();
                        displayMessage("\nSending packet containing: "
                                + message + "\n");

                        byte[] data = message.getBytes();

                        DatagramPacket sendPacket = new DatagramPacket(data,
                                data.length, InetAddress.getLocalHost(), 5000);
                        socket.send(sendPacket);
                        displayMessage("Packet sent\n");
                    } catch (IOException ioe) {
                        displayMessage(ioe + "\n");
                        ioe.printStackTrace();
                    }
                });
        add(enterField, BorderLayout.NORTH);

        displayArea = new JTextArea();
        displayArea.setFont(new Font("Arial", Font.PLAIN, 26));
        add(new JScrollPane(displayArea), BorderLayout.CENTER);

        setSize(600, 300);
        setVisible(true);

        try {
            socket = new DatagramSocket();
        } catch (SocketException se) {
            se.printStackTrace();
            System.exit(1);
        }
    }

    public void waitForPackets() {
        while (true) {
            try {
                byte[] data = new byte[100];
                DatagramPacket receivePacket = new DatagramPacket(data,
                        data.length);

                socket.receive(receivePacket);

                displayMessage("\nPacket received:"
                        + "\nFrom host: "
                        + receivePacket.getAddress()
                        + "\nHost port: "
                        + receivePacket.getPort()
                        + "\nLength: "
                        + receivePacket.getLength()
                        + "\nContaining: "
                        + new String(receivePacket.getData(), 0,
                                receivePacket.getLength()));
            } catch (IOException ioe) {
                displayMessage(ioe + "\n");
                ioe.printStackTrace();
            }
        }
    }

    private void displayMessage(String messageToDisplay) {
        SwingUtilities.invokeLater(() -> displayArea.append(messageToDisplay));
    }

    public static void main(String[] args) {
        DatagramClient application = new DatagramClient();
        application.setDefaultCloseOperation(EXIT_ON_CLOSE);
        application.waitForPackets();
    }

}
