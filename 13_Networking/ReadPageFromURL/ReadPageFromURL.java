import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ReadPageFromURL {

    public static void main(String[] args) {
        String input = JOptionPane.showInputDialog("URL to read");
        StringBuffer sb = new StringBuffer();
        if (input != null && !input.equals("")) {
            try {
                URL url = new URL(input);
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        url.openStream()));
                String tmp;
                while ((tmp = br.readLine()) != null) {
                    sb.append(tmp);
                    sb.append("\n");
                }
            } catch (MalformedURLException murle) {
                murle.printStackTrace();
                System.exit(1);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        JScrollPane disp = new JScrollPane(new JTextArea(sb.toString()));
        disp.setPreferredSize(new Dimension(600, 600));
        JOptionPane.showMessageDialog(null, disp, "Response from " + input,
                JOptionPane.PLAIN_MESSAGE);
    }
}
