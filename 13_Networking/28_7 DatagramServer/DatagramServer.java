import java.awt.BorderLayout;
import java.awt.Font;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

public class DatagramServer extends JFrame {
    private JTextArea displayArea;
    private DatagramSocket socket;

    public DatagramServer() {
        super("Server");

        displayArea = new JTextArea();
        displayArea.setFont(new Font("Arial", Font.PLAIN, 26));
        add(new JScrollPane(displayArea), BorderLayout.CENTER);
        setSize(600, 400);
        setVisible(true);

        try {
            socket = new DatagramSocket(5000);
        } catch (SocketException se) {
            se.printStackTrace();
            System.exit(1);
        }
    }

    public void waitForPackets() {
        while (true) {
            try {
                byte[] data = new byte[100];
                DatagramPacket receivePacket = new DatagramPacket(data,
                        data.length);

                socket.receive(receivePacket);

                displayMessage("\nPacket received:"
                        + "\nFrom host: "
                        + receivePacket.getAddress()
                        + "\nHost port: "
                        + receivePacket.getPort()
                        + "\nLength: "
                        + receivePacket.getLength()
                        + "\nContaining: "
                        + new String(receivePacket.getData(), 0,
                                receivePacket.getLength()));
                sendPacketToClient(receivePacket);
            } catch (IOException ioe) {
                displayMessage(ioe + "\n");
                ioe.printStackTrace();
            }
        }
    }

    private void sendPacketToClient(DatagramPacket receivePacket)
            throws IOException {
        displayMessage("\n\nEcho data to client....");

        DatagramPacket sendPacket = new DatagramPacket(receivePacket.getData(),
                receivePacket.getLength(), receivePacket.getAddress(),
                receivePacket.getPort());

        socket.send(sendPacket);
        displayMessage("Packet sent\n");
    }

    private void displayMessage(String messageToDisplay) {
        SwingUtilities.invokeLater(() -> displayArea.append(messageToDisplay));
    }

    public static void main(String[] args) {
        DatagramServer application = new DatagramServer();
        application.setDefaultCloseOperation(EXIT_ON_CLOSE);
        application.waitForPackets();
    }
}
