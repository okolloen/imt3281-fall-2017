package oppgave_22_10;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * Shows a JSlider and JTextField and synchronize the two, also displays the value in a panel.
 * 
 * @author oeivindk
 *
 */
public class SliderAndTextField extends JFrame {
	JSlider	slider = new JSlider(JSlider.HORIZONTAL, 0, 100, 25);
	JTextField text = new JTextField("25");
	PaintedPanel outputPanel = new PaintedPanel();
	
	/**
	 * Set up the GUI and add event listeners.
	 */
	public SliderAndTextField() {
		super ("Synchronized text field and slider");
		setLayout (new GridLayout(3, 1, 0, 0));
		add(slider);
		slider.addChangeListener(new ChangeListener() {	// Update the textfield and the output panel when the slider moves
			@Override
			public void stateChanged(ChangeEvent e) {
				text.setText(Integer.toString(slider.getValue()));
				outputPanel.setValue(slider.getValue());
			}
		});
		add(text);
		text.addActionListener(new ActionListener() {	// Update the slider and the output panel when the user inputs a new value
			@Override
			public void actionPerformed(ActionEvent e) {
				int value = Integer.parseInt(e.getActionCommand());
				slider.setValue(value);
				outputPanel.setValue(value);
			}
		});
		add(outputPanel);
		outputPanel.setValue(25);	// Default value on slider and textfield
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new SliderAndTextField();
	}
}
