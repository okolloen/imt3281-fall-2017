package oppgave_22_10;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * A panel that will show an int value using paintComponent.
 * 
 * @author oeivindk
 *
 */
public class PaintedPanel extends JPanel {
	int value = 0;

	/**
	 * Set a new value and display it
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
		repaint();
	}
	
	/**
	 * Draws the value given to this object centered in the panel.
	 * 
	 * @see java.awt.paintComponent(Graphics g)
	 */
	@Override
	public void paintComponent (Graphics g) {
		Dimension d = getSize();		// Get the size of the panel
		int width = g.getFontMetrics().stringWidth(Integer.toString(value));		// Get the width of the string
		int height = g.getFontMetrics().getHeight();								// Get the height of the string
		g.drawString(Integer.toString(value), d.width/2-width/2, d.height/2+height/2);
	}
}
