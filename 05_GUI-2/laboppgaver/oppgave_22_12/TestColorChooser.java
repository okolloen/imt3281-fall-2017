package oppgave_22_12;

import javax.swing.JFrame;

/**
 * Used to display and test the MyColorChooser class
 * 
 * @author oeivindk
 *
 */
public class TestColorChooser extends JFrame {
	public TestColorChooser() {
		super("Testing MyColorChooser");
		add (new MyColorChooser());
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new TestColorChooser();
	}
}
