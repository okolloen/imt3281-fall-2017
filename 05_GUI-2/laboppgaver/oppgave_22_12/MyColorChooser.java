package oppgave_22_12;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

/**
 * Objects of this class can be used to let a user select a color by specifying red/green/blue components
 * 
 * @author oeivindk
 *
 */
public class MyColorChooser extends JPanel {
	private JSlider redSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 0);
	private JSlider greenSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 0);
	private JSlider blueSlider = new JSlider(JSlider.HORIZONTAL, 0, 255, 0);
	private JTextField red = new JTextField("0", 3);
	private JTextField green = new JTextField("0", 3);
	private JTextField blue = new JTextField("0", 3);
	private JPanel preview = new JPanel();
	
	/**
	 * Add sliders and text field to GUI
	 */
	public MyColorChooser() {
		Box main = Box.createHorizontalBox();
		Box r = Box.createHorizontalBox();		// Combine slider and text field in a box
		r.add(redSlider);
		redSlider.addChangeListener(e->{			// Update text field when slider changes
			red.setText(Integer.toString(redSlider.getValue()));
			updatePreview();
		});
		r.add(red);
		red.addActionListener(e->{				// Update the slider when a new value has been entered
			redSlider.setValue(Integer.parseInt(e.getActionCommand()));
		});
		Box g = Box.createHorizontalBox();
		g.add(greenSlider);
		greenSlider.addChangeListener(e->{
			green.setText(Integer.toString(greenSlider.getValue()));
			updatePreview();
		});
		g.add(green);
		green.addActionListener(e->{				// Update the slider when a new value has been entered
			greenSlider.setValue(Integer.parseInt(e.getActionCommand()));
		});
		Box b = Box.createHorizontalBox();
		b.add(blueSlider);
		blueSlider.addChangeListener(e->{
			blue.setText(Integer.toString(blueSlider.getValue()));
			updatePreview();
		});
		b.add(blue);
		blue.addActionListener(e->{				// Update the slider when a new value has been entered
			blueSlider.setValue(Integer.parseInt(e.getActionCommand()));
		});

		Box sliders = Box.createVerticalBox();	// Place the sliders and text field below each other
		sliders.add(r);
		sliders.add(g);
		sliders.add(b);
		main.add(sliders);						// Add the sliders and text fields to the left
		main.add(new JLabel("  "));				// Add a bit of space between the text fields and the preview
		main.add(preview);						// Add the preview to the right
		preview.setOpaque(true);					// Must be opaque to show background color
		preview.setPreferredSize(new Dimension(60, 60));
		preview.setBackground(Color.BLACK);
		add(main);
	}

	private void updatePreview() {
		preview.setBackground(new Color(redSlider.getValue(), greenSlider.getValue(), blueSlider.getValue()));
	}
}
