# Welcome to IMT3281 Application development #

In a folder of your choosing, do

```
#!bash

git clone https://bitbucket.org/okolloen/imt3281-fall-2017.git
```

Before any lecture do 

```
#!bash

git pull
```
to ensure you have the latest version of the lecture notes. See [the progress plan](https://trello.com/b/0CL0kQGK/imt3281-h17) for information about the topics of any given lecture.