package no.hig.okolloen.CoverViewer;

import javafx.scene.image.Image;

public class Book {
    private String title;
    private Image thumbImage;
    private Image largeImage;

    public Book(String title, String thumbPath, String largePath) {
        this.title = title;
        thumbImage = new Image(getClass().getResourceAsStream(thumbPath));
        largeImage = new Image(getClass().getResourceAsStream(largePath));
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the thumbImage
     */
    public Image getThumbImage() {
        return thumbImage;
    }

    /**
     * @param thumbImage
     *            the thumbImage to set
     */
    public void setThumbImage(Image thumbImage) {
        this.thumbImage = thumbImage;
    }

    /**
     * @return the largeImage
     */
    public Image getLargeImage() {
        return largeImage;
    }

    /**
     * @param largeImage
     *            the largeImage to set
     */
    public void setLargeImage(Image largeImage) {
        this.largeImage = largeImage;
    }

    public String toString() {
        return title;
    }
}
