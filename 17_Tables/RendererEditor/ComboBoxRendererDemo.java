import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class ComboBoxRendererDemo extends JFrame {
    JComboBox<Integer> combo = new JComboBox<>();

    public ComboBoxRendererDemo() {
        DefaultComboBoxModel<Integer> items = new DefaultComboBoxModel<>();
        for (int i = 0; i < 14; i++)
            items.addElement(new Integer(i + 1));
        combo.setModel(items);
        combo.setRenderer(new MyRenderer());
        add(combo);
        combo.setPreferredSize(new Dimension(88, 88));
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    class MyRenderer extends JLabel implements ListCellRenderer<Object> {
        public MyRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList<?> list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {
            Color background;
            Color foreground;

            if (isSelected) {
                background = Color.RED;
                foreground = Color.WHITE;

                // unselected, and not the DnD drop location
            } else {
                background = Color.WHITE;
                foreground = Color.BLACK;
            }
            ;

            setIcon(new ImageIcon(getClass().getResource("/images/" + value.toString() + ".jpg")));

            setBackground(background);
            setForeground(foreground);

            return this;
        }
    }

    public static void main(String[] args) {
        new ComboBoxRendererDemo();

    }

}
