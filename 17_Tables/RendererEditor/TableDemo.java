import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.TableCellRenderer;

public class TableDemo extends JFrame {
    MyDataModel model = new MyDataModel();
    JTable table = new JTable(model);

    public TableDemo() {
        super("Table with model and renderers demo");
        model.addRow();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setMoodColumnEditor(); // How the column is edited
        setMoodColumnRenderer(); // How the column is rendered
        table.setRowHeight(88);
        table.getColumnModel().getColumn(0).setPreferredWidth(220);
        table.getColumnModel().getColumn(1).setPreferredWidth(88);
        table.getColumnModel().getColumn(2).setPreferredWidth(388);
        add(new JScrollPane(table));
        setSize(700, 400);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void setMoodColumnEditor() {
        JComboBox<Integer> combo = new JComboBox<>();
        combo.setPreferredSize(new Dimension(88, 88));
        DefaultComboBoxModel<Integer> items = new DefaultComboBoxModel<>();
        for (int i = 0; i < 14; i++)
            items.addElement(new Integer(i + 1));
        combo.setModel(items);
        table.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(combo));
        combo.setRenderer(new ComboBoxRenderer()); // How the editor is rendered

    }

    private void setMoodColumnRenderer() {
        table.getColumnModel().getColumn(1).setCellRenderer(new MoodColumnRenderer());
    }

    public static void main(String[] args) {
        new TableDemo();
    }

    public class MoodColumnRenderer extends JLabel implements TableCellRenderer {

        public MoodColumnRenderer() {
            super();
            setOpaque(true);
        }

        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {

            if (isSelected) {
                setBackground(table.getSelectionBackground());
            } else {
                setBackground(table.getBackground());
            }

            setIcon(new ImageIcon(getClass().getResource("/images/" + value.toString() + ".jpg")));

            return this;
        }
    }

    class ComboBoxRenderer extends JLabel implements ListCellRenderer<Object> {
        public ComboBoxRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList<?> list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {

            if (isSelected) {
                setBackground(Color.RED);
            } else {
                setBackground(list.getBackground());
            }

            setIcon(new ImageIcon(getClass().getResource("/images/" + value.toString() + ".jpg")));

            return this;
        }
    }
}
