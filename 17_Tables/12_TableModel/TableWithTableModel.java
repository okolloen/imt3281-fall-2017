import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class TableWithTableModel extends JFrame {
	public TableWithTableModel() {
		super ("Værvarsel for Gjøvik");
		add (new JScrollPane(new JTable(new MyTableModel())));
		setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		TableWithTableModel demo = new TableWithTableModel();
		demo.setVisible(true);
	}
	
	class MyTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		String headings[] = {"Tidspunkt", "Værtype(kode)", "Værtype", "Nedbør", "Vindrettning", "Vindhastighet", "Vindhastighet", "temperatur"};
		Vector<Object[]> data = no.ntnu.imt3281.yrVarsel.YrData.getWeather();

		@Override
        public String getColumnName(int col) {
            return headings[col];
        }
		
		@Override
		public int getRowCount() {
			return data.size();
		}

		@Override
		public int getColumnCount() {
			return headings.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return data.elementAt(rowIndex)[columnIndex];
		}
		
		/*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then everything would be rendered as text.
         */
		@Override
        public Class<?> getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }
		
		/*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col == 0) {	// Do not allow user to edit date/time
                return false;
            } else {
                return true;
            }
        }
        
        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            data.elementAt(row)[col] = value;
            fireTableCellUpdated(row, col);	// Notify the view that the contents of the model has changed
        }
	}
}
