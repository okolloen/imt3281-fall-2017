import java.awt.Component;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

@SuppressWarnings("serial")
public class TableWithEditor extends JFrame {
	private String directions[] = {"N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"};
	
	public TableWithEditor() {
		super ("Værvarsel for Gjøvik");
		JTable table = new JTable(new MyTableModel());
		add (new JScrollPane(table));
		table.getColumnModel().getColumn(1).setCellRenderer(new WeatherRenderer());
		table.getColumnModel().getColumn(3).setCellRenderer(new WeatherRenderer());
		JComboBox<String> directionsEditor = new JComboBox<>(directions);								// Create editor
		table.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(directionsEditor));	// Add as editor
		
		// directionsEditor.setRenderer(new DirectionsEditorRenderer());
		table.getColumnModel().getColumn(0).setPreferredWidth(150);
		table.getColumnModel().getColumn(1).setPreferredWidth(50);
		table.getColumnModel().getColumn(3).setPreferredWidth(50);
		
		table.setRowHeight(30);
		setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		TableWithEditor demo = new TableWithEditor();
		demo.setVisible(true);
	}
	
	/**
	 * Used to render content of wind direction combo box
	 */
	class DirectionsEditorRenderer implements ListCellRenderer<String> {

		@Override
		public Component getListCellRendererComponent(JList<? extends String> list, String value, int index,
				boolean isSelected, boolean cellHasFocus) {
			JLabel label = new JLabel();
			ImageIcon icon = new ImageIcon(getClass().getResource("/30/"+String.valueOf(value)+".png"));
			if (isSelected) {
				label.setBackground(list.getSelectionBackground());
				label.setOpaque(true);
			}
			label.setIcon(icon);
			return label;
		}
	}
	
	
	/**
	 * Used to render column 1, the weather type
	 * @author oeivindk
	 *
	 */
	class WeatherRenderer extends DefaultTableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
				int row, int column) {
			JLabel label = new JLabel();
			// The value of this column contains a code which is the name of a png image in the folder 30 (30px is the size of the
			// images.)
			ImageIcon icon = new ImageIcon(getClass().getResource("/30/"+String.valueOf(value)+".png"));
			if (isSelected) {
				label.setBackground(table.getSelectionBackground());
				label.setOpaque(true);
			}
			label.setIcon(icon);
			return label;
		}
	}
	
	class MyTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		String headings[] = {"Tidspunkt", "Værtype", "Nedbør", "Vindrettning", "Vindhastighet", "Vindhastighet", "temperatur"};
		Vector<Object[]> data = no.ntnu.imt3281.yrVarsel.YrData.getWeather();

		@Override
        public String getColumnName(int col) {
            return headings[col];
        }
		
		@Override
		public int getRowCount() {
			return data.size();
		}

		@Override
		public int getColumnCount() {
			// We skip the duplicate weather type
			return headings.length;
		}

		@Override
		public Object getValueAt(int rowIndex, int columnIndex) {
			return data.elementAt(rowIndex)[columnIndex>1?columnIndex+1:columnIndex];
		}
		
		/*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then everything would be rendered as text.
         */
		@Override
        public Class<?> getColumnClass(int columnIndex) {
            return getValueAt(0, columnIndex).getClass();
        }
		
		/*
         * Don't need to implement this method unless your table's
         * editable.
         */
        public boolean isCellEditable(int row, int columnIndex) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (columnIndex == 0) {	// Do not allow user to edit date/time
                return false;
            } else {
                return true;
            }
        }	
        
        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int columnIndex) {
            data.elementAt(row)[columnIndex>1?columnIndex+1:columnIndex] = value;
            fireTableCellUpdated(row, columnIndex);	// Notify the view that the contents of the model has changed
        }
	}
}
