package no.ntnu.imt3281.yrVarsel;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Used to get weather data from yr.no. Parses the XML data returned and returns it as an array of weather objects.
 * 
 * @author oeivindk
 *
 */
public class YrData {
	private static String location = "https://www.yr.no/sted/Norge/Oppland/Gj%C3%B8vik/Gj%C3%B8vik/varsel.xml";
	
	private YrData () { }
	
	/**
	 * Set the location to fetch weather data for. Go to the given location on yr.no, add "varsel.xml" to the url
	 * and use that as the location parameter. 
	 * 
	 * @param location the URL for the location to fetch weather data from.
	 */
	public void setLocqtion (String location) {
		YrData.location = location;
	}
	
	/**
	 * Get weather information for the given location and returns a vector containing arrays of objects with 
	 * weather predictions for different time intervals.
	 * Each array of objects contains the data/time for the prediction and a cloud cover string, a cloud cover code, precipitation,
	 * wind direction, wind speed (as text), wind speed (as mps) and the temperature. 
	 * 
	 * @return a vector with weather predictions.
	 */
	public static Vector<Object[]> getWeather () {
		Vector<Object[]> data = new Vector<Object[]>();
		try {
			URL input = new URL(location);
			InputStream is = input.openStream();
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(is);
			doc.getDocumentElement().normalize();
			XPath xPath =  XPathFactory.newInstance().newXPath();
			String expression = "//tabular/time";	        
			NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				
				// Get from attribute, change format of time from "yyyy-MM-ddTHH:mm:ss" to "dd-MM-yyyy HH:mm"
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
				LocalDateTime dt = LocalDateTime.from(dtf.parse(nNode.getAttributes().getNamedItem("from").getNodeValue()));
				dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
				String formattedDateTime = dt.format(dtf);
				
				// Get information about weather for this time
				Element e = (Element)nNode;
				String cloudCoverString = e.getElementsByTagName("symbol").item(0).getAttributes().getNamedItem("name").getNodeValue();
				String cloudCoverCode = e.getElementsByTagName("symbol").item(0).getAttributes().getNamedItem("var").getNodeValue();
				Integer precipitation = Integer.parseInt(e.getElementsByTagName("precipitation").item(0).getAttributes().getNamedItem("value").getNodeValue());
				String windDirection = e.getElementsByTagName("windDirection").item(0).getAttributes().getNamedItem("code").getNodeValue();
				String windSpeed = e.getElementsByTagName("windSpeed").item(0).getAttributes().getNamedItem("name").getNodeValue();
				Float mps = Float.parseFloat(e.getElementsByTagName("windSpeed").item(0).getAttributes().getNamedItem("mps").getNodeValue());
				Integer temperature = Integer.parseInt(e.getElementsByTagName("temperature").item(0).getAttributes().getNamedItem("value").getNodeValue());
				Object weaterData[] = {formattedDateTime, cloudCoverCode, cloudCoverString, precipitation, windDirection, windSpeed, mps, temperature};
				data.add(weaterData);
			}
		} catch (MalformedURLException e) {
			System.err.println("Badly formated location given");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("Unable to read from the location given");
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			System.out.println("Unable to create XML parser");
			e.printStackTrace();
		} catch (SAXException e) {
			System.err.println("Unable to parse document at " + location);
			e.printStackTrace();
		} catch (XPathExpressionException e) {
			System.err.println("Badly formated XPath expression");
			e.printStackTrace();
		} catch (DOMException e) {
			System.out.println("Unable to create DOM from document at " + location);
			e.printStackTrace();
		}
		return data;
	}
	
	public static void main(String[] args) {
		Vector<Object[]> data = YrData.getWeather();
		for (Object[] prediction : data) {
			System.out.printf("%15s%15s%15s %-5s%2dC%n", prediction[0], prediction[2], prediction[5], prediction[4], prediction[7]);
		}
	}
}
