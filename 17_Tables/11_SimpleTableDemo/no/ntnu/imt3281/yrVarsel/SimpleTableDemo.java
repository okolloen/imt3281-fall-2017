package no.ntnu.imt3281.yrVarsel;

import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

@SuppressWarnings("serial")
public class SimpleTableDemo extends JFrame {
	JTable table;
	
	public SimpleTableDemo() {
		super ("Værvarsel for Gjøvik");
		String headings[] = {"Tidspunkt", "Værtype(kode)", "Værtype", "Nedbør", "Vindrettning", "Vindhastighet", "Vindhastighet", "temperatur"};
		Vector<Object[]> weatherData = no.ntnu.imt3281.yrVarsel.YrData.getWeather();	// Fetch vector with weather predictions
		
		// Convert the vector to an array (containing arrays, since the vector contains arrays.)
		Object[][] data = new Object[weatherData.size()][headings.length];
		int i=0;
		for (Object[] forecast : weatherData) {
			data[i++] = forecast;
		}
		
		// Create the table, data is a two dimensional array containing the data to be presented.
		table = new JTable(data, headings);
		add (new JScrollPane(table));
		setSize(640, 480);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args) {
		SimpleTableDemo demo = new SimpleTableDemo();
		demo.setVisible(true);
	}
}
