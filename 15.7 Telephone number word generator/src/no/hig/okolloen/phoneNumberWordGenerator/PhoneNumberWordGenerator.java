package no.hig.okolloen.phoneNumberWordGenerator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A program to generate words from phone numbers. Exercise on sept. 10, 2015.
 * @author oivindk
 *
 */
public class PhoneNumberWordGenerator extends JFrame {
	JTextField phoneNumber;		// GUI Field for entering phone number
	JRadioButton english;		// Let the user select english dictionary
	JRadioButton norwegian;		// Let the user select norwegian dictionary
	JButton generateWords;		// Button to initiate the generation of words
	JTextArea wordList;			// Textarea to present the generated words
	Vector<String> dictionary = new Vector<String>();	// Dictionary for the words currently being generated
	Vector<String> generatedWordList;					// List of words currently generated
	StringBuffer currentWord = new StringBuffer();		// String that being manipulated as words are being generated
	char characters[][] = new char[8][4];				// Array containing letters for the numbers 2-9
	
	/**
	 * Generate user interface for the application. Has no functionality
	 * but will provide the user with a user interface to use the application.
	 */
	public PhoneNumberWordGenerator () {
		super ("Phone number word generator");
		setLayout(new BorderLayout (5,5));	// Override default borderlayout to generate space between panels
		// Add some space around the outermost components
		((JPanel)getContentPane()).setBorder(BorderFactory.createEmptyBorder(10, 15, 5, 5));

		// Create separate panel for the list of generated words
		// This is just so that we can add a label above the textarea
		// to explain what the textarea is for.
		// This panel will be added to the center of the frame
		JPanel wordListPanel = new JPanel (new BorderLayout());
		wordListPanel.add(new JLabel ("Words generated"), BorderLayout.NORTH);
		wordListPanel.add(new JScrollPane(wordList = new JTextArea(20, 50)));
		add (wordListPanel, BorderLayout.CENTER);
		wordList.setEditable(false);
		wordList.setForeground(Color.BLACK);

		// Create a panel for the two radio buttons that let the user 
		// choose the dictionary to use. Add a border to the panel explaining
		// what the intention of the radio buttons actually is.
		// This panel will be added below the image showing the key layout
		// of the phone, to the east of the frame
		JPanel selectDictionary = new JPanel(new GridLayout(2, 1));
		selectDictionary.add(english = new JRadioButton("English"));
		selectDictionary.add(norwegian = new JRadioButton("Norwegian", true));
		ButtonGroup group = new ButtonGroup();
		group.add (english);
		group.add (norwegian);
		selectDictionary.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), 
						"Select dictionary"));
		selectDictionary.setPreferredSize(new Dimension(200, 60));

		// Using a box layout to arrange the image of the key layout of the phone and
		// the radio buttons to select dictionary below each other and place that panel
		// to the east in the frame
		Box rightPane = Box.createVerticalBox();
		rightPane.add(Box.createVerticalStrut(20));
		rightPane.add(new JLabel(new ImageIcon(getClass().getResource("/images/phoneKeys.jpg"))));
		rightPane.add(selectDictionary);
		add(rightPane, BorderLayout.EAST);

		// Use a panel to place components to the south of the frame. 
		// This panel contains a label, a text field and a button. This is
		// where the user will enter the phone number, it also has the button
		// pressed to generate the list of words.
		JPanel phoneNumberPanel = new JPanel(new BorderLayout(5, 0));
		phoneNumberPanel.add(new JLabel("Enter phone number "), BorderLayout.WEST);
		phoneNumberPanel.add(phoneNumber = new JTextField(20), BorderLayout.CENTER);
		phoneNumberPanel.add(generateWords = new JButton("Generate words"), BorderLayout.EAST);
		add (phoneNumberPanel, BorderLayout.SOUTH);

		// Add action listener to the textfield, pressing enter will
		// generate the list of words
		phoneNumber.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generateWords();
			}
		});
		// Add action listener to the button, pressing it will
		// generate the list of words
		generateWords.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				generateWords ();
			}
		});
		
		// pack means arrange the components with best fit.
		pack ();
		setVisible(true);
		// Set ut the array containing the characters for the digits from 2-9
		setupCharacters();
	}

	/** 
	 * This method just initializes the array with the letters for the digits 
 	 * from 2-9. The value of 0 (zero) is used to indicate that the fourth element
	 * for most digits are unused. 
	 * */
	private void setupCharacters() {
		characters[0][0] = 'A';
		characters[0][1] = 'B';
		characters[0][2] = 'C';
		characters[0][3] = 0;
		characters[1][0] = 'D';
		characters[1][1] = 'E';
		characters[1][2] = 'F';
		characters[1][3] = 0;
		characters[2][0] = 'G';
		characters[2][1] = 'H';
		characters[2][2] = 'I';
		characters[2][3] = 0;
		characters[3][0] = 'J';
		characters[3][1] = 'K';
		characters[3][2] = 'L';
		characters[3][3] = 0;
		characters[4][0] = 'M';
		characters[4][1] = 'N';
		characters[4][2] = 'O';
		characters[4][3] = 0;
		characters[5][0] = 'P';
		characters[5][1] = 'Q';
		characters[5][2] = 'R';
		characters[5][3] = 'S';
		characters[6][0] = 'T';
		characters[6][1] = 'U';
		characters[6][2] = 'V';
		characters[6][3] = 0;
		characters[7][0] = 'W';
		characters[7][1] = 'X';
		characters[7][2] = 'Y';
		characters[7][3] = 'Z';
	}

	/** 
	 * Method called when the button are pressed or enter are pressed in the
	 * textfield.
	 * The method that a valid number is entered (only digits 2-9 are used), then 
	 * convert those digits into corresponding values. I.e., the character '2'
	 * will be transformed to the digit 0 (since '2' will have its letters stored
	 * in index 0 in the characters array.)
	*/ 
	protected void generateWords() {
		if (phoneNumber.getText().matches("[2-9]+")) {	// Only enter if valid value
			// Create a new char array of the same length as the entered number
			char digits[] = new char[phoneNumber.getText().length()];
			// Convert the string to an array of chars
			digits = phoneNumber.getText().toCharArray();
			currentWord = new StringBuffer();		// This will contain the word being generated
			for (int i=0; i<digits.length; i++) {	
				digits[i] = (char) (digits[i]-'2');	// Convert from String form to number, '2'=0
				currentWord.append(characters[digits[i]][0]);	// Add the first letter for that digit to the string
			}
			dictionary.clear();		// The user can change dictionary and length of number between runs, so always start fresh
			readWordList();			// Read selected dictionary and get the words with the right length
			wordList.setText("");	// Clear the list of words show to the user
			generatedWordList = new Vector<String>();	// Clear the list of words used to avoid duplicates
			wordGenerator (digits, 0);	// generate the words, using the entered numbers, start with altering the first letter
			if (wordList.getText().length()==0)
				JOptionPane.showMessageDialog(this, "Try a different number", "No words generated", JOptionPane.WARNING_MESSAGE);
		} else	// Not a phone number
			JOptionPane.showMessageDialog(this, "Phone number must be digits 2-9 only", "Bad number entered", JOptionPane.ERROR_MESSAGE);
	}

	/**
	 * Method used to read the selected list of words and save the words with the 
	 * correct length.
	 */
	private void readWordList() {
		try {
			InputStream is = null;
			if (norwegian.isSelected())	// User selected norwegian language
				is = getClass().getResourceAsStream("/norwegian.txt");
			else							// User selected english
				is = getClass().getResourceAsStream("/english.txt");
			// Read the file one line at a time
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String tmp = null;
			int length = phoneNumber.getText().length();
			while ((tmp = br.readLine())!=null) {	// As long as not EOF
				if (tmp.length() == length)			// If correct length
					dictionary.add(tmp.toUpperCase());	// Add to dictionary, note, use upper case 
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Recursive method used to generate all permutations of the letters
	 * for the different digits.
	 * 
	 * @param digits the digits (numbers) entered, as an array with 0 being "2" and 7 being "9"
	 * @param letterPos for what position in the array are we to change the letters
	 */
	private void wordGenerator(char[] digits, int letterPos) {
		for (int i=0; i<4; i++) {	// Max number of letters for a digit
			if (characters[digits[letterPos]][i]!=0) {	// If i is pointing to a letter
				// Change the letter on desired position to given letter
				currentWord.setCharAt(letterPos, characters[digits[letterPos]][i]);
				if (!generatedWordList.contains(currentWord.toString())) {	// This is a combination of letter not previously generated
					generatedWordList.add(currentWord.toString());			// Add it to list of generated combination
					if (dictionary.contains(currentWord.toString())) {		// This is an actual word
						wordList.append(currentWord.toString());				// Show this word to the user
						wordList.append("\n");
					}
				}
				if (letterPos<digits.length-1)			// There are more positions in the array
					wordGenerator(digits, letterPos+1);	// Call the method again to permutate the rest of the digits
			}
		}
	}

	/**
	 * Starts the program
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new PhoneNumberWordGenerator();
		
	}
}
