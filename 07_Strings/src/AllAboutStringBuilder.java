// Fig 14.10 - 14.14 combined

public class AllAboutStringBuilder {
	public static void main(String[] args) {
		// ************ Constructors **************
		System.out.println("************ Constructors **************");
		StringBuilder buffer1 = new StringBuilder();
		StringBuilder buffer2 = new StringBuilder(10);
		StringBuilder buffer3 = new StringBuilder("hello");
		
		System.out.printf("buffer1 = \"%s\"\n", buffer1);
		System.out.printf("buffer2 = \"%s\"\n", buffer2);
		System.out.printf("buffer3 = \"%s\"\n", buffer3);
		
		// ************ Capacity/length **************
		System.out.println("************ Capacity/length **************");
		StringBuilder buffer = new StringBuilder("Hello, how are you?");
		
		System.out.printf("buffer = %s\nlength = %d\ncapacity = %d\n\n",
				buffer.toString(), buffer.length(), buffer.capacity());
		
		buffer.ensureCapacity(75);
		System.out.printf("New capacity = %d\n\n", buffer.capacity());
		
		buffer.setLength(10);
		System.out.printf("New length = %d\nbuffer = %s\n", 
				buffer.length(), buffer.toString());

		// ************* Characters ********************
		System.out.println("************* Characters ********************");
		buffer = new StringBuilder("hello there");
		
		System.out.printf("buffer = %s\n", buffer.toString());
		System.out.printf("Character at 0: %s\nCharacter at 4: %s\n\n", 
				buffer.charAt(0), buffer.charAt(4));
		
		char[] charArray = new char[buffer.length()];
		buffer.getChars(0, buffer.length(), charArray, 0);
		System.out.print("The characters are: ");
		
		for (char character : charArray)
			System.out.print(character);
		
		buffer.setCharAt(0, 'H');
		buffer.setCharAt(6, 'T');
		System.out.printf("\n\nbuffer = %s", buffer.toString());
		
		buffer.reverse();
		System.out.printf("\n\nbuffer = %s\n", buffer.toString());

		// ************ Append **********
		System.out.println("************ Append **********");
		Object objectRef = "hello"; 
		String string = "goodbye";  
		char[] charArray1 = {'a', 'b', 'c', 'd', 'e', 'f'};
		boolean booleanValue = true;
		char characterValue = 'Z';
		int integerValue = 7;
		long longValue = 10000000000L;
		float floatValue = 2.5f; 
		double doubleValue = 33.333;
		
		StringBuilder lastBuffer = new StringBuilder("last buffer");
		buffer = new StringBuilder();
		
		buffer.append(objectRef)
				.append("%n")
				.append(string)
				.append("%n")
				.append(charArray1)
				.append("%n")
				.append(charArray1, 0, 3)
				.append("%n")
				.append(booleanValue)
				.append("%n")
				.append(characterValue)
				.append("%n")
				.append(integerValue)
				.append("%n")
				.append(longValue)
				.append("%n")
				.append(floatValue)
				.append("%n")
				.append(doubleValue)
				.append("%n")
				.append(lastBuffer);

		System.out.printf("buffer contains%n%s%n", buffer.toString());

		// ************ Insert/delete **************
		System.out.println("************ Insert/delete **************");
		
		buffer = new StringBuilder();
		
		buffer.insert(0, objectRef)
			.insert(0, "  ") // each of these contains new line
			.insert(0, string)
			.insert(0, "  ")
			.insert(0, charArray1)
			.insert(0, "  ")
			.insert(0, charArray1, 3, 3)
			.insert(0, "  ")
			.insert(0, booleanValue)
			.insert(0, "  ")
			.insert(0, characterValue)
			.insert(0, "  ")
			.insert(0, integerValue)
			.insert(0, "  ")
			.insert(0, longValue)
			.insert(0, "  ")
			.insert(0, floatValue)
			.insert(0, "  ")
			.insert(0, doubleValue);
		
		System.out.printf("buffer after inserts:\n%s\n\n", buffer.toString());
		
		buffer.deleteCharAt(10); // delete 5 in 2.5
		buffer.delete(2, 6); // delete .333 in 33.333
		
		System.out.printf("buffer after deletes:\n%s\n", buffer.toString());

	}
}
