// Fig 14.1 - 14.9 combined into one big file

public class AllAboutStrings {
	public static void main(String[] args) {
		// ******** Constructors ********
		System.out.println("******** Constructors ********");
		char[] charArray = {'b', 'i', 'r', 't', 'h', ' ', 'd', 'a', 'y'};
		String s = new String("hello");
		
		// use String constructors
		String s1 = new String();
		String s2 = new String(s);
		String s3 = new String(charArray);
		String s4 = new String(charArray, 6, 3);
		
		System.out.printf("s1 = %s\ns2 = %s\ns3 = %s\ns4 = %s\n", s1, s2, s3, s4); 

		// ******** Miscellaneous *********
		System.out.println("******** Miscellaneous *********");
		s1 = "hello there";
		charArray = new char[5];
		
		System.out.printf("s1: %s", s1);
		
		// test length method
		System.out.printf("\nLength of s1: %d", s1.length());
		
		// loop through characters in s1 with charAt and display reversed
		System.out.printf("%nThe string reversed is: ");
		
		for (int count = s1.length() - 1; count >= 0; count--)
			System.out.printf("%c ", s1.charAt(count));
		
		// copy characters from string into charArray
		s1.getChars(0, 5, charArray, 0);
		System.out.printf("%nThe character array is: ");
		
		for (char character : charArray)
			System.out.print(character);
		
		System.out.println();

		// ********** String compare **********
		System.out.println("********** String compare **********");
		s1 = new String("hello"); // s1 is a copy of "hello"
		s2 = "goodbye";
		s3 = "Happy Birthday";
		s4 = "happy birthday";
		
		System.out.printf("s1 = %s\ns2 = %s\ns3 = %s\ns4 = %s\n\n", s1, s2, s3, s4);
		
		// test for equality
		if (s1.equals("hello"))  // true
			System.out.println("s1 equals \"hello\"");
		else
			System.out.println("s1 does not equal \"hello\""); 

		// test for equality with ==
		if (s1 == "hello")  // false; they are not the same object
			System.out.println("s1 is the same object as \"hello\"");
		else
			System.out.println("s1 is not the same object as \"hello\"");
		
		// test for equality (ignore case)
		if (s3.equalsIgnoreCase(s4))  // true
			System.out.printf("%s equals %s with case ignored\n", s3, s4);
		else
			System.out.println("s3 does not equal s4");

		// test compareTo
		System.out.printf("\ns1.compareTo(s2) is %d", s1.compareTo(s2));
		System.out.printf("\ns2.compareTo(s1) is %d", s2.compareTo(s1));
		System.out.printf("\ns1.compareTo(s1) is %d", s1.compareTo(s1));
		System.out.printf("\ns3.compareTo(s4) is %d", s3.compareTo(s4));
		System.out.printf("\ns4.compareTo(s3) is %d\n\n", s4.compareTo(s3));
		
		// test regionMatches (case sensitive)
		if (s3.regionMatches(0, s4, 0, 5))
			System.out.println("First 5 characters of s3 and s4 match");
		else
			System.out.println("First 5 characters of s3 and s4 do not match");

		// test regionMatches (ignore case)
		if (s3.regionMatches(true, 0, s4, 0, 5))
			System.out.println("First 5 characters of s3 and s4 match with case ignored");
		else
			System.out.println("First 5 characters of s3 and s4 do not match");

		// ********* Start/end **********
		System.out.println("********* Start/end **********");
		String[] strings = {"started", "starting", "ended", "ending"};
		
		// test method startsWith
		for (String string : strings) {
		     if (string.startsWith("st"))
		        System.out.printf("\"%s\" starts with \"st\"\n", string);
		} 

		System.out.println();
		
		// test method startsWith starting from position 2 of string
		for (String string : strings) {
		     if (string.startsWith("art", 2)) 
		        System.out.printf(
		           "\"%s\" starts with \"art\" at position 2\n", string);
		} 

		System.out.println();
		
		// test method endsWith
		for (String string : strings) {
		     if (string.endsWith("ed"))
		        System.out.printf("\"%s\" ends with \"ed\"\n", string);
		} 

		// ********** Index methods ***********
		System.out.println("********** Index methods ***********");
		String letters = "abcdefghijklmabcdefghijklm";
		
		// test indexOf to locate a character in a string
		System.out.printf("'c' is located at index %d\n", letters.indexOf('c'));
		System.out.printf("'a' is located at index %d\n", letters.indexOf('a', 1));
		System.out.printf("'$' is located at index %d\n\n", letters.indexOf('$'));
		
		// test lastIndexOf to find a character in a string
		System.out.printf("Last 'c' is located at index %d\n", letters.lastIndexOf('c'));
		System.out.printf("Last 'a' is located at index %d\n", letters.lastIndexOf('a', 25));
		System.out.printf("Last '$' is located at index %d\n\n", letters.lastIndexOf('$'));
		
		// test indexOf to locate a substring in a string
		System.out.printf("\"def\" is located at index %d\n", letters.indexOf("def"));
		System.out.printf("\"def\" is located at index %d\n", letters.indexOf("def", 7));
		System.out.printf("\"hello\" is located at index %d\n\n", letters.indexOf("hello"));
		
		// test lastIndexOf to find a substring in a string
		System.out.printf("Last \"def\" is located at index %d\n", letters.lastIndexOf("def"));
		System.out.printf("Last \"def\" is located at index %d\n", letters.lastIndexOf("def", 25));
		System.out.printf("Last \"hello\" is located at index %d\n", letters.lastIndexOf("hello"));

		// ********** Substrings ***********
		System.out.println("********** Substrings ***********");
		letters = "abcdefghijklmabcdefghijklm";
		
		// test substring methods
		System.out.printf("Substring from index 20 to end is \"%s\"\n", letters.substring(20));
		System.out.printf("%s \"%s\"\n", 
				"Substring from index 3 up to, but not including 6 is",
				letters.substring(3, 6));

		// ********* Concatenation **********
		System.out.println("********* Concatenation **********");
		s1 = "Happy ";
		s2 = "Birthday";
		
		System.out.printf("s1 = %s\ns2 = %s\n\n",s1, s2);
		System.out.printf(
				"Result of s1.concat(s2) = %s\n", s1.concat(s2));
		System.out.printf("s1 after concatenation = %s\n", s1);

		// ********** Miscellaneous 2 ***********
		System.out.println("********** Miscellaneous 2 ***********");
		s1 = "hello";
		s2 = "GOODBYE";
		s3 = "   spaces   ";
		
		System.out.printf("s1 = %s\ns2 = %s\ns3 = %s\n\n", s1, s2, s3);
		
		// test method replace      
		System.out.printf(
				"Replace 'l' with 'L' in s1: %s\n\n", s1.replace('l', 'L'));
		
		// test toLowerCase and toUpperCase
		System.out.printf("s1.toUpperCase() = %s\n", s1.toUpperCase());
		System.out.printf("s2.toLowerCase() = %s\n\n", s2.toLowerCase());
		
		// test trim method
		System.out.printf("s3 after trim = \"%s\"\n\n", s3.trim());
		
		// test toCharArray method
		charArray = s1.toCharArray();
		System.out.print("s1 as a character array = ");
		
		for (char character : charArray)
			System.out.print(character);
		
		System.out.println();

		// *********** valueOf ************
		System.out.println("*********** valueOf ************");
		char[] charArray1 = {'a', 'b', 'c', 'd', 'e', 'f'};
		boolean booleanValue = true;
		char characterValue = 'Z';
		int integerValue = 7;
		long longValue = 10000000000L; // L suffix indicates long
		float floatValue = 2.5f; // f indicates that 2.5 is a float
		double doubleValue = 33.333; // no suffix, double is default
		Object objectRef = "hello"; // assign string to an Object reference
		
		System.out.printf("char array = %s\n", String.valueOf(charArray1));
		System.out.printf("part of char array = %s\n", String.valueOf(charArray1, 3, 3));
		System.out.printf("boolean = %s\n", String.valueOf(booleanValue));
		System.out.printf("char = %s\n", String.valueOf(characterValue));
		System.out.printf("int = %s\n", String.valueOf(integerValue));
		System.out.printf("long = %s\n", String.valueOf(longValue)); 
		System.out.printf("float = %s\n", String.valueOf(floatValue)); 
		System.out.printf("double = %s\n", String.valueOf(doubleValue)); 
		System.out.printf("Object = %s\n", String.valueOf(objectRef));

	}
}
