import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Vector;

import javax.swing.JFileChooser;


public class ReadWriteTextFiles {

	private static Vector<String> readTextFile(File selectedFile) {
		Vector<String> vector = new Vector<String>();
		try {
			// FileInputStream is a raw, byte based input stream
			FileInputStream fis = new FileInputStream(selectedFile);
			// InputStreamReader is a character based stream
			InputStreamReader isr = new InputStreamReader(fis);
			// BufferedReader allows us to read a line at a time
			BufferedReader br = new BufferedReader(isr);
			String tmp = null;
			// readLine reads until EOL or EOF is reached.
			while ((tmp = br.readLine())!=null) {
				// Add the lines to the vector
				vector.add(tmp);
			}
			// Close the file
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return vector;
	}
	
	private static void writeTextFile(Vector<String> lines) {
		try {
			// Opens a stream from writing
			FileOutputStream fos = new FileOutputStream("testing.tmp");
			// Wrap the byte stream in a character stream and then wrap
			// the character stream in a buffered stream.
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			// Loop over the lines in the vector
			for (String line: lines) {
				// Write the line, note, the EOL was removed by br.readLine()
				bw.write(line);
				// Add EOL, will automatically add correct EOL depending on platform.
				bw.newLine();
			}
			// Close the file (important, the file WILL NOT be correctly
			// written if you forget this.)
			bw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int result = fileChooser.showOpenDialog(null);

		// if user clicked Cancel button on dialog, return
		if (result == JFileChooser.CANCEL_OPTION)
			System.exit(1);

		// Get the file and read from it
		Vector<String> lines = readTextFile (fileChooser.getSelectedFile());
		writeTextFile(lines);
	}
}
