/**
 * 
 */
package classNStuff;

import javax.swing.JOptionPane;

/**
 * Class representing a rectangle. All rectangles have a height and a width. 
 * The constructor asks for these values when objects are created.
 * 
 * @author oivindk
 *
 */
public class Rectangle extends Shape {
	private double height, width;
	
	/**
	 * Initiates a new rectangle, asks the user for height and width.
	 */
	public Rectangle () {
		height = Double.parseDouble(JOptionPane.showInputDialog("Height of rectangle"));
		width = Double.parseDouble(JOptionPane.showInputDialog("Width of rectangle"));
	}

	/**
	 * Get the area of the rectangle by multiplying the height by the width.
	 * 
	 * @return the area of the rectangle.
	 */
	@Override
	public double getArea () {
		return height*width;
	}
	
	@Override
	public String toString() {
		return "Et rektangel med høyde: "+height+" og bredde: "+width+" har et areal på "+getArea();
	}
}