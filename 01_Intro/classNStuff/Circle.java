/**
 * 
 */
package classNStuff;

import javax.swing.JOptionPane;

/**
 * Class representing a circle. All circles have a radius.
 * The constructor asks the user to give the radius when the class is
 * created.
 * 
 * @author oivindk
 *
 */
public class Circle extends Shape {
	private double radius;
	
	/**
	 * Creates a new instance of the circle object and get the
	 * radius from the user
	 */
	public Circle () {
		radius = Double.parseDouble(
					JOptionPane.showInputDialog("Radius of the circle"));
	}

	/**
	 * Get the area of the circle.
	 * 
	 * @return the area of the circle.
	 */
	@Override
	public double getArea() {
		return radius*radius*Math.PI;
	}
	
	@Override
	public String toString() {
		return "En sirkel med radius: "+radius+" har et areal på "+getArea();
	}
}
