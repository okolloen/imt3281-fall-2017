/**
 * 
 */
package classNStuff;

import java.util.Vector;

/**
 * Quick test of different shapes.
 * 
 * @author oivindk
 *
 */
public class TestShapes {
	Vector<Shape> shapes = new Vector<Shape>();
	
	public TestShapes () {
		for (int i=0; i<5; i++) {	// Create five shapes
			if (i%2==0) {
				Circle c = new Circle();
				shapes.add(c);
			} else {
				Rectangle r = new Rectangle();
				shapes.add(r);
			}
		}
		for (Shape shape: shapes) {	// Go through all shapes and print their areas
			if (shape instanceof Circle) {
				System.out.println("En sirkel med areal: " + shape.getArea());
			} else if (shape instanceof Rectangle) {
				System.out.println("Et rektangel med areal: " + shape.getArea());
			}
		}
		for (Shape shape: shapes) { // Go through shapes and print info
			System.out.println(shape);
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestShapes();
	}

}
