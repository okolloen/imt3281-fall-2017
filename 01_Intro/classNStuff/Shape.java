/**
 * 
 */
package classNStuff;

/**
 * This is the base class for all shapes.
 * @author oivindk
 *
 */
abstract public class Shape {
	/**
	 * By making this method abstract classes that extends this class
	 * must specify an implementation for this method.
	 * This method should return the area of the shape.
	 * 
	 * @return the area of a given shape.
	 */
	abstract double getArea ();
}
