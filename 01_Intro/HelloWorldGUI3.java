import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * 
 */


/**
 * Hello world example with image of the world. 
 * Using import of needed classed
 * 
 * @author oivindk
 *
 */
public class HelloWorldGUI3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, new ImageIcon("world.jpg"), "Hello world", JOptionPane.PLAIN_MESSAGE);
	}

}
