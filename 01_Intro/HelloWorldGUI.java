/**
 * 
 */

/**
 * Graphical version of Hello World
 * 
 * @author oivindk
 *
 */
public class HelloWorldGUI {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		javax.swing.JOptionPane.showMessageDialog(null, "Hello world");
	}

}
