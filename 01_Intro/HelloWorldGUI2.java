/**
 * 
 */
import javax.swing.*;

/**
 * More advanced graphical version of Hello World, using import of javax.swing.*.
 * @author oivindk
 *
 */
public class HelloWorldGUI2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JOptionPane.showMessageDialog(null, "Hello world", "Hello World", JOptionPane.PLAIN_MESSAGE);
	}

}
