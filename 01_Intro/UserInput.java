import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * How to get user input with JOptionPane. 
 *
 * @author oivindk
 *
 */
public class UserInput {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String name = JOptionPane.showInputDialog("Hva heter du?");
		JOptionPane.showMessageDialog(null, "Hei "+name);
	}

}
