/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * Objects of this class represents equilateral triangles.
 * @author oivindk
 *
 */
public class Triangle extends TwoDimensionalShape {
	private double sides = 1;
	
	/**
	 * Create a new triangle where all sides have a length of 1.
	 */
	public Triangle () {
		super();
	}

	/**
	 * Create a new triangle where all sides have the given length.
	 * 
	 * @param sides the length of the sides of this triangle.
	 */
	public Triangle (double sides) {
		this.sides = sides;
	}
	
	/**
	 * Get the area of this triangle<br>
	 * <img src="doc-files/Area-of-Triangle.png" alt="Formulas for calculating area of triangle">
	 * 
	 * @see oppgaver.oppg_10_13.TwoDimensionalShape#getArea()
	 * @return the area of this triangle
	 */
	@Override
	public double getArea() {
		return .5*sides*sides*Math.sin(Math.PI/3);
	}

	/**
	 * @return the length of the sides of this triangle.
	 */
	public double getSides() {
		return sides;
	}

	/**
	 * @param sides the length of the sides of this triangle.
	 */
	public void setSides(double sides) {
		this.sides = sides;
	}

	/**
	 * Return information about this triangle.
	 * 
	 * @see java.lang.Object#toString()
	 * @return the length of the sides and area of this triangle.
	 */
	@Override
	public String toString() {
		return "triangle [sides=" + sides + "; area=" + getArea() + "]";
	}
}
