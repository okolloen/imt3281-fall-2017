/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * Objects of this class represents circles.
 * 
 * @author oivindk
 *
 */
public class Circle extends TwoDimensionalShape {
	private double radius = 1;
	
	/**
	 * Creates a circle with radius = 1.
	 */
	public Circle () {
		super ();
	}
	
	/**
	 * Creates a circle with the given radius.
	 * 
	 * @param radius the radius for the cirlce being created.
	 */
	public Circle (double radius) {
		this.radius = radius;
	}

	/** 
	 * Calculates the area of this circle : PI*radius^2.
	 * 
	 * @see oppgaver.oppg_10_13.TwoDimensionalShape#getArea()
	 * @return the area of this cirlce.
	 */
	@Override
	public double getArea() {
		return radius*radius*Math.PI;
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/** 
	 * Returns information about this circle as a string.
	 * 
	 * @see java.lang.Object#toString()
	 * @return a string with this circles radius and area.
	 */
	@Override
	public String toString() {
		return "Circle [radius=" + radius + "; area=" + getArea()+ "]";
	}
}