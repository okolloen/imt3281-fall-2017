/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * A cube where all sides have the same length.
 * @author oivindk
 *
 */
public class Cube extends ThreeDimensionalShape {
	private double sides = 1;
	
	/**
	 * Create a new cube where all sides have a length of 1.
	 */
	public Cube () {
		super();
	}

	/**
	 * Create a new cub where all sides have the given length.
	 * 
	 * @param sides the length of the sides of this cube.
	 */
	public Cube (double sides) {
		this.sides = sides;
	}
	
	/**
	 * Get the surface area of this cube, length of sides squared times 6.
	 * @see oppgaver.oppg_10_13.ThreeDimensionalShape#getArea()
	 * @return the surface area of this cube
	 */
	@Override
	public double getArea() {
		return sides*sides*6;
	}

	/**
	 * @return the length of the sides of this cube.
	 */
	public double getSides() {
		return sides;
	}

	/**
	 * @param sides the length of the sides of this cube.
	 */
	public void setSides(double sides) {
		this.sides = sides;
	}

	/**
	 * Return information about this cube.
	 * 
	 * @see java.lang.Object#toString()
	 * @return the length of the sides, the surface area and volume of this cube.
	 */
	@Override
	public String toString() {
		return "Cube [sides=" + sides + "; surce area=" + getArea() + "; volume="+ getVolume() + "]";
	}

	/**
	 * Get the volume of this cube.
	 * @see oppgaver.oppg_10_13.ThreeDimensionalShape#getVolume()
	 * @return the volume of this cube
	 */
	@Override
	public double getVolume() {
		return sides*sides*sides; 
	}
}
