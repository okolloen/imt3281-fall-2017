/**
 * 
 */
package oppgaver.oppg_10_13;

import java.util.Vector;

/**
 * @author oivindk
 *
 */
public class TestShapes {
	static Vector<Shape> shapes = new Vector<>();
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		shapes.add(new Circle(2));
		shapes.add(new Sphere(2));
		shapes.add(new Square(2));
		shapes.add(new Cube(2));
		shapes.add(new Triangle(2));
		shapes.add(new Tetrahedron(2));
		for (Shape shape: shapes) {
			System.out.println(shape);
			if (shape instanceof TwoDimensionalShape) {
				System.out.println("This two dimensional shape has an area of " + 
									((TwoDimensionalShape)shape).getArea());
			} else if (shape instanceof ThreeDimensionalShape) {
				System.out.println("This three dimensional shape has a surface area of "+
									((ThreeDimensionalShape)shape).getArea()+" and a volume of "+
									((ThreeDimensionalShape)shape).getVolume());
			}
		}
	}

}
