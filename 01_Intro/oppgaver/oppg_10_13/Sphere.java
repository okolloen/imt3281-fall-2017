/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * Objects of this class represents perfect spheres.
 * 
 * @author oivindk
 *
 */
public class Sphere extends ThreeDimensionalShape {
	private double radius=1;
	
	/**
	 * Creates a sphere with radius = 1.
	 */
	public Sphere () {
		super ();
	}
	
	/**
	 * Creates a sphere with the given radius.
	 * 
	 * @param radius the radius for the sphere being created.
	 */
	public Sphere (double radius) {
		this.radius = radius;
	}

	
	/** 
	 * Calculates the surface area of this sphere : 4*PI*radius^2.
	 * 
	 * @see oppgaver.oppg_10_13.TwoDimensionalShape#getArea()
	 * @return the surface area of this sphere.
	 */
	@Override
	public double getArea() {
		return radius*radius*Math.PI;
	}
	
	/**
	 * Calculates the volume of this sphere : (4/3)*PI*radius^3
	 *  
	 * @see oppgaver.oppg_10_13.ThreeDimensionalShape#getVolume()
	 * @return the volume of this sphere.
	 */
	@Override
	public double getVolume() {
		return (4/3)*Math.PI*radius*radius*radius;
	}

	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		this.radius = radius;
	}

	/** 
	 * Returns information about this sphere as a string.
	 * 
	 * @see java.lang.Object#toString()
	 * @return a string with this speheres radius, surface area and volume.
	 */
	@Override
	public String toString() {
		return "Sphere [radius=" + radius + "; area=" + getArea()+ "; volume=" + getVolume() + "]";
	}
}
