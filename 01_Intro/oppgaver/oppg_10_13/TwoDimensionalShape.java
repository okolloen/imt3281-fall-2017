/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * All two dimensional shapes should inherit this class.
 * 
 * @author oivindk
 *
 */
public abstract class TwoDimensionalShape extends Shape {
	/**
	 * All two dimensional shapes should be able to return their area.
	 * 
	 * @return the area of this shape.
	 */
	public abstract double getArea();
}
