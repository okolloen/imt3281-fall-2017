/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * A regular tetrahedron where all sides have the same length.
 * @author oivindk
 *
 */
public class Tetrahedron extends ThreeDimensionalShape {
	private double sides = 1;
	
	/**
	 * Create a new tetrahedron where all sides have a length of 1.
	 */
	public Tetrahedron () {
		super();
	}

	/**
	 * Create a new tetrahedron where all sides have the given length.
	 * 
	 * @param sides the length of the sides of this tetrahedron.
	 */
	public Tetrahedron (double sides) {
		this.sides = sides;
	}
	
	/**
	 * Get the surface area of this tetrahedron, length of sides squared times 6.
	 * Four times that of the triangle : <br/>
	 * <img src="doc-files/Area-of-Triangle.png" alt="Formulas for calculating area of triangle">
	 * 
	 * @see oppgaver.oppg_10_13.ThreeDimensionalShape#getArea()
	 * @return the surface area of this tetrahedron
	 */
	@Override
	public double getArea() {
		return .5*sides*sides*Math.sin(Math.PI/3)*4;
	}

	/**
	 * @return the length of the sides of this tetrahedron.
	 */
	public double getSides() {
		return sides;
	}

	/**
	 * @param sides the length of the sides of this tetrahedron.
	 */
	public void setSides(double sides) {
		this.sides = sides;
	}

	/**
	 * Return information about this tetrahedron.
	 * 
	 * @see java.lang.Object#toString()
	 * @return the length of the sides, the surface area and volume of this tetrahedron.
	 */
	@Override
	public String toString() {
		return "Tetrahedron [sides=" + sides + "; surce area=" + getArea() + "; volume="+ getVolume() + "]";
	}

	/**
	 * Get the volume of this tetrahedron.
	 * {@link https://en.wikipedia.org/wiki/Tetrahedron#Volume}
	 * 
	 * @see oppgaver.oppg_10_13.ThreeDimensionalShape#getVolume()
	 * @return the volume of this tetrahedron.java
	 */
	@Override
	public double getVolume() {
		return sides*sides*sides/(6*Math.sqrt(2)); 
	}
}
