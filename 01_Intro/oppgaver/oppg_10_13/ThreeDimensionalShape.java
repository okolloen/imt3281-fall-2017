/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * All tree dimensional shapes should inherit this class.
 * 
 * @author oivindk
 *
 */
public abstract class ThreeDimensionalShape extends Shape {
	/**
	 * All tree dimensional shapes should be able to return their surface area.
	 * 
	 * @return the surface area of this shape.
	 */
	public abstract double getArea();
	
	/**
	 * All tree dimensional shapes should be able to return their volume.
	 * 
	 * return the volume of this shape.
	 */
	public abstract double getVolume();
}
