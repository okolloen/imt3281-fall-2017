/**
 * 
 */
package oppgaver.oppg_10_13;

/**
 * A rectangle where all sides have the same length.
 * @author oivindk
 *
 */
public class Square extends TwoDimensionalShape {
	private double sides = 1;
	
	/**
	 * Create a new square where all sides have a length of 1.
	 */
	public Square () {
		super();
	}

	/**
	 * Create a new square where all sides have the given length.
	 * 
	 * @param sides the length of the sides of this square.
	 */
	public Square (double sides) {
		this.sides = sides;
	}
	
	/**
	 * Get the area of this square, length of sides squared.
	 * @see oppgaver.oppg_10_13.TwoDimensionalShape#getArea()
	 * @return the area of this square
	 */
	@Override
	public double getArea() {
		return sides*sides;
	}

	/**
	 * @return the length of the sides of this square.
	 */
	public double getSides() {
		return sides;
	}

	/**
	 * @param sides the length of the sides of this square.
	 */
	public void setSides(double sides) {
		this.sides = sides;
	}

	/**
	 * Return information about this square.
	 * 
	 * @see java.lang.Object#toString()
	 * @return the length of the sides and area of this square.
	 */
	@Override
	public String toString() {
		return "Square [sides=" + sides + "; area=" + getArea() + "]";
	}
}
