package oppgaver.oppg_8_4;

/**
 * Objects of this class represents cylinders. A cylinder has a radius and a height.
 * 
 * @author oivindk
 *
 */
public class Cylinder {
	private double height=1;
	private double radius=1;
	
	/**
	 * @return the height
	 */
	public double getHeight() {
		return height;
	}
	/**
	 * @param height the height to set
	 */
	public void setHeight(double height) {
		if (height>0)
			this.height = height;
	}
	/**
	 * @return the radius
	 */
	public double getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(double radius) {
		if (radius>0)
			this.radius = radius;
	}
	
	/**
	 * @return the volume of the cylinder
	 */
	public double getVolume() {
		return radius*radius*height*Math.PI;
	}
}
