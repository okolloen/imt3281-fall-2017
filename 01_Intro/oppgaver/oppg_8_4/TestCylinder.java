package oppgaver.oppg_8_4;

public class TestCylinder {

	public static void main(String[] args) {
		Cylinder cylinder = new Cylinder();
		System.out.println("A cylinder width radius=1 and height=1 has a volume of " + cylinder.getVolume());
		cylinder.setHeight(2);
		cylinder.setRadius(2);
		System.out.println("The cylinder now has a radius of " + cylinder.getRadius() + " and a height of "
				+ cylinder.getHeight());
		cylinder.setHeight(-1);
		cylinder.setRadius(-1);
		System.out.println("The cylinder still has a radius of " + cylinder.getRadius() + " and a height of "
				+ cylinder.getHeight());
		System.out.println("The volume of the cylinder is " + cylinder.getVolume());
	}

}
