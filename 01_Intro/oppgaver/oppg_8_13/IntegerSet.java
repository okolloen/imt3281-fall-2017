package oppgaver.oppg_8_13;

public class IntegerSet {
	boolean[] set = new boolean[101];
	
	public IntegerSet() {
		for (int i=0; i<set.length; i++)
			set[i] = false;
	}
	
	static IntegerSet union (IntegerSet set1, IntegerSet set2) {
		IntegerSet res = new IntegerSet();
		for (int i=0; i<set1.set.length; i++) {
			res.set[i] = set1.set[i]||set2.set[i];
		}
		return res;
	}
	
	static IntegerSet intersection (IntegerSet set1, IntegerSet set2) {
		IntegerSet res = new IntegerSet();
		for (int i=0; i<set1.set.length; i++) {
			res.set[i] = set1.set[i]&&set2.set[i];
		}
		return res;
	}
	
	public void insertElement (int element) {
		set[element] = true;
	}
	
	public void deleteElement (int element) {
		set[element] = false;
	}
	
	@Override
	public String toString () {
		String tmp = "";
		for (int i=0; i<set.length; i++)
			if (set[i])
				tmp += i+" ";
		tmp = tmp.trim();
		return (tmp.length()==0)?"---":tmp;
	}
}