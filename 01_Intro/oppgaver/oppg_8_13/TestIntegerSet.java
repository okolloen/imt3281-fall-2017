package oppgaver.oppg_8_13;

public class TestIntegerSet {

	public static void main(String[] args) {
		IntegerSet one = new IntegerSet ();
		System.out.println("Empty set : " + one);
		System.out.println("Adding 3, 5, 19, 45 to set");
		one.insertElement(3);
		one.insertElement(5);
		one.insertElement(19);
		one.insertElement(45);
		System.out.println("Set now contains : " + one);
		IntegerSet two = new IntegerSet();
		two.insertElement(5);
		two.insertElement(6);
		two.insertElement(20);
		System.out.println("Created new set with contents : " + two);
		System.out.println("Union of these two sets : " + IntegerSet.union(one, two));
		System.out.println("Intersection of these two sets : " + IntegerSet.intersection(one, two));
		two.deleteElement(20);
		System.out.println("Removing element 20 from second set, it now contains : " + two);
	}

}
