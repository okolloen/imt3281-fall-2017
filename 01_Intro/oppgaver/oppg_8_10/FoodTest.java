package oppgaver.oppg_8_10;

public class FoodTest {
	public static void main(String[] args) {
		System.out.println(	"All food types");
		for (Food food: Food.values()) {	// Loop through all types of food.
			System.out.printf("%-10s%-15s%s\n", food, food.getType(), food.getCalories());
		}
		Food f = Food.APPLE;	// Create a single enum instance
	}
}
