package oppgaver.oppg_8_10;

public enum Food {
	APPLE("fruit", 23),
	BANANA("fruid", 43),
	CARROT("vegetable", 34);
	
	private final String type;
	private final int calories;
	
	Food(String type, int calories) {
		this.type = type;
		this.calories = calories;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the calories
	 */
	public int getCalories() {
		return calories;
	}
}
