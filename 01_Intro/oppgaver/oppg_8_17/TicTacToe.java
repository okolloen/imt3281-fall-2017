package oppgaver.oppg_8_17;

import javax.swing.JOptionPane;

public class TicTacToe {
	Cell[][] board = new Cell[3][3];
	String player1 = "";
	String player2 = "";
	AIPlayerMinimax ai = new AIPlayerMinimax(board);
	int move=0;
	
	public TicTacToe () {
		for (int row=0; row<board.length; row++) {
			for (int col=0; col<board[row].length; col++) {
				board[row][col] = Cell.EMPTY;
			}
		}
		printBoard();
		player1 = JOptionPane.showInputDialog("Name of player 1, leave blank for computer");
		if (player1.equals("")) {
			player1="Computer";
		}
		player2 = JOptionPane.showInputDialog("Name of player 2, leave blank for computer");
		if (player2.equals("")) {
			player2="Computer";
		}
		while (!gameOver()) {	// Keep playing until one player wins or no more moves.
			if (move%2==0) {
				boolean moved = false;
				do {
					int[] rowCol;
					if (player1.equals("Computer")) {	// Computer player, let the AI do its magic
						ai.setSeed(Cell.X);
						rowCol = ai.move();
					} else {							// Human player, get move from player
						rowCol = getPosition(player1);	
					}
					if (board[rowCol[0]][rowCol[1]]==Cell.EMPTY) {
						board[rowCol[0]][rowCol[1]] = Cell.X;
						moved = true;
					} else {
						JOptionPane.showMessageDialog(null, "That location on the board is already occupied", "Unable to place piece here", JOptionPane.PLAIN_MESSAGE);
					}
				} while (!moved);
			} else {
				boolean moved = false;
				do {
					int[] rowCol;
					if (player2.equals("Computer")) {	// Computer player, let the AI do its magic
						ai.setSeed(Cell.O);
						rowCol = ai.move();
					} else {							// Human player, get move from player
						rowCol = getPosition(player2);	
					}
					if (board[rowCol[0]][rowCol[1]]==Cell.EMPTY) {
						board[rowCol[0]][rowCol[1]] = Cell.O;
						moved = true;
					} else {
						JOptionPane.showMessageDialog(null, "That location on the board is already occupied", "Unable to place piece here", JOptionPane.PLAIN_MESSAGE);
					}
				} while (!moved);			
			}
			move++;
			printBoard();
		}
	}
	
	/**
	 * Checks to see if one of the players has won the game or the game is a tie.
	 * 
	 * @return false if the game is still on, or true if the game is over.
	 */
	private boolean gameOver () {
		for (int row=0; row<board.length; row++) {	// Three in a row
			if (board[row][0]!=Cell.EMPTY && board[row][0]==board[row][1] && board[row][1]==board[row][2]) {
				System.out.println("\n\nPlayer " + ((board[row][0] == Cell.X) ? player1 : player2) + " won the game!");
				return true;
			}
		}
		for (int col=0; col<board[0].length; col++) {	// Three in a column
			if (board[0][col]!=Cell.EMPTY && board[0][col]==board[1][col] && board[1][col]==board[2][col]) {
				System.out.println("\n\nPlayer " + ((board[0][col] == Cell.X) ? player1 : player2) + " won the game!");
				return true;
			}
		}
		if (board[0][0]!=Cell.EMPTY && board[0][0]==board[1][1] && board[1][1]==board[2][2]) { // Top left to bottom right
			System.out.println("\n\nPlayer " + ((board[0][0] == Cell.X) ? player1 : player2) + " won the game!");
			return true;
		}
		if (board[2][0]!=Cell.EMPTY && board[2][0]==board[1][1] && board[1][1]==board[0][2]) { // Bottom left to top right
			printBoard();
			System.out.println("\n\nPlayer " + ((board[0][0] == Cell.X) ? player1 : player2) + " won the game!");
			return true;
		}
		if (move==9) {	// No winner
			System.out.println("\n\nIt's a tie");
			return true;
		} else	// More moves available
			return false;
	}
	
	/**
	 * Get the grid position to place a piece in from the user. 
	 * Reads column (A, B, C) and row (1, 2, 3) and returns it as an two element
	 * array where the first element is the row and the second element is the column.
	 * 
	 * @param playerName the name of the player, used in title of the dialog
	 * @return an array where the first element is the row and the second element is the column to place the players piece.
	 */
	private int[] getPosition (String playerName) {
		int[] res = new int[2];
		boolean complete;
		do {
			complete = true;
			String tmp = JOptionPane.showInputDialog(null, "Where to place your piece (A1 is top let, B2 is center etc", "Player : "+playerName, JOptionPane.PLAIN_MESSAGE);
			if (tmp.length()==2) {
				if (tmp.charAt(0)=='A') {
					res[1] = 0;
				} else if (tmp.charAt(0)=='B') {
					res[1] = 1;
				} else if (tmp.charAt(0)=='C') {
					res[1] = 2;
				} else
					complete = false;
				if (tmp.charAt(1)=='1') {
					res[0] = 0;
				} else if (tmp.charAt(1)=='2') {
					res[0] = 1;
				} else if (tmp.charAt(1)=='3') {
					res[0] = 2;
				} else 
					complete = false;
			}
		} while (!complete);
		return res;
	}
	
	/**
	 * For centering output when printing, single character strings
	 * get padded with two spaces front and back.
	 * 
	 * @param input the string to pad
	 * @return the padded string.
	 */
	private String pad (String input) {
		return (input.length()==1)?"  "+input+"  ":input;
	}
	
	/**
	 * Prints the tic tac toe board with column (A,B,C) and row (1,2,3) markers
	 */
	private void printBoard () {
		System.out.printf("\n%-5s%-7s%-7s%-7s\n", "", pad("A"), pad("B"), pad("C"));
		System.out.printf("%-5s%-7s%-7s%-7s\n", "1", pad(board[0][0].name()), pad(board[0][1].name()), pad(board[0][2].name()));
		System.out.printf("%-5s%-7s%-7s%-7s\n", "2", pad(board[1][0].name()), pad(board[1][1].name()), pad(board[1][2].name()));
		System.out.printf("%-5s%-7s%-7s%-7s\n", "3", pad(board[2][0].name()), pad(board[2][1].name()), pad(board[2][2].name()));
	}
	
	public static void main(String[] args) {
		TicTacToe ticTacToe = new TicTacToe();
		ticTacToe.printBoard();
	}
	
}
