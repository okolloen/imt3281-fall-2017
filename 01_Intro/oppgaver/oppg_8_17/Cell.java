package oppgaver.oppg_8_17;

public enum Cell {
	X("player 1"),
	O("player 2"),
	EMPTY("empty");
	
	private final String player;
	Cell (String player) {
		this.player = player;
	}
	
	public String getPlayer() {
		return player;
	}
}
