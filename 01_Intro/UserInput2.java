import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * Get numbers from JOptionPane.
 * @author oivindk
 *
 */
public class UserInput2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String tmp = JOptionPane.showInputDialog("Tall 1");
		int number1 = Integer.parseInt(tmp);
		tmp = JOptionPane.showInputDialog("Tall 2");
		int number2 = Integer.parseInt(tmp);
		tmp = "The sum of "+number1+" and "+number2+" is "+(number1+number2);
		JOptionPane.showMessageDialog(null, tmp);
	}

}
