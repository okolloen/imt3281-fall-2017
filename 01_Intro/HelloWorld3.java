/**
 * 
 */

/**
 * This class is the first demo, the famous Hello World program
 * 
 * @author oivindk
 *
 */
public class HelloWorld3 {

	/**
	 * @param args parameters sent from the command line
	 */
	public static void main(String[] args) {
		System.out.println("Hello world");
	}

}
