/**
 * 
 */
package no.ntnu.imt3281.laboppgave;

import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

/**
 * This class acts as a contact registry, it uses objects of class Contact to store information about up to 
 * ten different contacts.
 * 
 * @author oeivindk
 *
 */
public class ContactRegistry {
	private Contact contacts[] = new Contact[10];
	private int registeredContacts = 0;
	public static ResourceBundle messages;
	public static Locale currentLocale;

	/**
	 * Default constructor, will ask the user to fill inn the information for all contacts
	 * using JOptionPane.inputDialog.
	 */
	public ContactRegistry() {
		String addMoreContacts = "N";
		do {
			String fname, lname, phone, bdate;
			fname = JOptionPane.showInputDialog(messages.getString("requestFirstName"));
			if (fname!=null&&!fname.equals("")) {
				lname = JOptionPane.showInputDialog(messages.getString("requestLastName"));
				if (lname!=null&&!lname.equals("")) {
					phone = JOptionPane.showInputDialog(messages.getString("requestPhoneNumber"));
					if (phone!=null&!phone.equals("")) {
						bdate = JOptionPane.showInputDialog(messages.getString("requestBDate"));
						
						// Code below from https://docs.oracle.com/javase/tutorial/datetime/iso/format.html
						try {
						    DateTimeFormatter formatter =
						                      DateTimeFormatter.ofPattern(messages.getString("inputDateFormat"));
						    LocalDate date = LocalDate.parse(bdate, formatter);
							Contact contact = new Contact(fname, lname, phone, date);
							contacts[registeredContacts++] = contact;
						}
						catch (DateTimeParseException exc) {
						    System.out.printf("%s is not parsable!%n", bdate);
						    // throw exc;      // Rethrow the exception.
						}
						// TODO ask for birth date, handle formating with I18N
					}
				}
			}
			addMoreContacts = JOptionPane.showInputDialog("Add another contact? (y/n)");
		} while (addMoreContacts!=null&&addMoreContacts.equalsIgnoreCase("y"));
	}
	
	/**
	 * Dumps the contents of the contact registry to standard output.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		int actualContacts = 0;
		for (int i=0; i<contacts.length; i++) {	// Find number of non null contacts
			if (contacts[i]!=null)
				actualContacts++;
		}
		
		// From https://docs.oracle.com/javase/tutorial/i18n/format/choiceFormat.html
		MessageFormat messageForm = new MessageFormat("");
		messageForm.setLocale(currentLocale);
		
		double[] contactLimits = {0,1,2};
		String [] contactStrings = {
		    messages.getString("noContacts"),
		    messages.getString("oneContact"),
		    messages.getString("moreContacts")
		};

		ChoiceFormat choiceForm = new ChoiceFormat(contactLimits, contactStrings);
		
		String pattern = messages.getString("registerToStringPattern");
		messageForm.applyPattern(pattern);
		
		Format[] formats = {choiceForm, null, NumberFormat.getInstance()};
		messageForm.setFormats(formats);
		
		String contactsString = "";
		for (Contact contact : contacts) {	// Loop through all contacts, add the ones that are not null
			if (contact!=null) {
				contactsString += contact+"-----\n";
			}
		}
		
		Object[] messageArguments = {actualContacts, actualContacts, contactsString};
		
		String result = messageForm.format(messageArguments);
		
		return result;
	}


	public static void main(String[] args) {
		String language;
        String country;

        if (args.length != 2) {
            language = new String("en");
            country = new String("US");
        } else {
            language = new String(args[0]);
            country = new String(args[1]);
        }

        // https://docs.oracle.com/javase/8/docs/api/java/util/Locale.html
        currentLocale = new Locale(language, country);
        
        System.out.println(currentLocale.toString() + ": " + currentLocale.getDisplayName());

        // https://docs.oracle.com/javase/tutorial/i18n/resbundle/index.html
        messages = ResourceBundle.getBundle("no.ntnu.imt3281.laboppgave.I18N", currentLocale);
        
		ContactRegistry registry = new ContactRegistry();
		System.out.println(registry);
	}
}
