package introExample;

import java.util.*;

public class I18NSample {

    static public void main(String[] args) {

        String language;
        String country;

        if (args.length != 2) {
            language = new String("en");
            country = new String("US");
        } else {
            language = new String(args[0]);
            country = new String(args[1]);
        }

        // https://docs.oracle.com/javase/8/docs/api/java/util/Locale.html
        Locale currentLocale;
        ResourceBundle messages;

        currentLocale = new Locale(language, country);
        
        System.out.println(currentLocale.toString() + ": " + currentLocale.getDisplayName());

        // https://docs.oracle.com/javase/tutorial/i18n/resbundle/index.html
        messages = ResourceBundle.getBundle("introExample.MessagesBundle", currentLocale);
        System.out.println(messages.getString("greetings"));
        System.out.println(messages.getString("inquiry"));
        System.out.println(messages.getString("farewell"));
    }
}