package formatting;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

public class Formatting {
	
	/**
	 * @see https://docs.oracle.com/javase/tutorial/i18n/format/numberFormat.html
	 */
	static public void displayNumber(Locale currentLocale) {

	    Integer quantity = new Integer(123456);
	    Double amount = new Double(345987.246);
	    NumberFormat numberFormatter;
	    String quantityOut;
	    String amountOut;

	    numberFormatter = NumberFormat.getNumberInstance(currentLocale);
	    quantityOut = numberFormatter.format(quantity);
	    amountOut = numberFormatter.format(amount);
	    System.out.println(quantityOut + "   " + currentLocale.toString());
	    System.out.println(amountOut);
	}
	
	/**
	 * @see https://docs.oracle.com/javase/tutorial/i18n/format/numberFormat.html
	 */
	static public void displayCurrency( Locale currentLocale) {

	    Double currencyAmount = new Double(9876543.21);
	    Currency currentCurrency = Currency.getInstance(currentLocale);
	    NumberFormat currencyFormatter = 
	        NumberFormat.getCurrencyInstance(currentLocale);

	    System.out.println(
	        currentLocale.getDisplayName() + ", " +
	        currentCurrency.getDisplayName() + ": " +
	        currencyFormatter.format(currencyAmount));
	    System.out.println(currentCurrency.getDisplayName()+", "+
	        currentCurrency.getSymbol());
	}
	
	/**
	 * @see https://docs.oracle.com/javase/tutorial/i18n/format/numberFormat.html
	 */
	static public void displayPercent(Locale currentLocale) {

	    Double percent = new Double(0.75);
	    NumberFormat percentFormatter;
	    String percentOut;

	    percentFormatter = NumberFormat.getPercentInstance(currentLocale);
	    percentOut = percentFormatter.format(percent);
	    System.out.println(percentOut);
	}
	
	/**
	 * @see https://docs.oracle.com/javase/tutorial/i18n/format/dateFormat.html
	 */
	static public void datesAndTimes(Locale currentLocale) {
		Date today = new Date();
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, currentLocale);
		// Formatting style : DEFAULT, SHORT, MEDIUM, LONG, FULL
		System.out.println(dateFormatter.format(today));
		
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.DEFAULT, currentLocale);
		System.out.println(timeFormatter.format(today));
		
		DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT,  DateFormat.SHORT, currentLocale);
		System.out.println(formatter.format(today));
	}
	
	/**
	 * @see https://docs.oracle.com/javase/tutorial/i18n/format/messageFormat.html
	 */
	static public void compoundMessages(Locale currentLocale) {
		ResourceBundle messages = ResourceBundle.getBundle("formatting.MessageBundle", currentLocale);
		Object[] messageArguments = {
			    messages.getString("planet"),
			    new Integer(7),
			    new Date()
			};
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(currentLocale);
		formatter.applyPattern(messages.getString("template"));
		System.out.println(formatter.format(messageArguments));	
	}
	
	public static void main(String[] args) {
		Locale locales[] = new Locale[5];
		locales[0] = new Locale.Builder().setLanguage("en").setRegion("GB").build();
		locales[1] = new Locale.Builder().setLanguage("en").setRegion("US").build();
		locales[2] = new Locale.Builder().setLanguage("fr").setRegion("FR").build();
		locales[3] = new Locale.Builder().setLanguage("de").setRegion("DE").build();
		locales[4] = new Locale.Builder().setLanguage("no").setRegion("NO").build();
		
		for (Locale currentLocale : locales) {
			displayNumber(currentLocale);
			displayCurrency(currentLocale);
			displayPercent(currentLocale);
			datesAndTimes(currentLocale);
			compoundMessages(currentLocale);
			System.out.println("================");
		}
	}
}