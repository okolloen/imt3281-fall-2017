/**
 * Sample Skeleton for 'Painter.fxml' Controller Class
 */

package no.hig.okolloen.painter;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class PainterController {

    private enum PenSize {
        SMALL(2), MEDIUM(4), LARGE(6);

        private final int radius;

        PenSize(int radius) {
            this.radius = radius;
        }

        public int getRadius() {
            return radius;
        }
    }

    @FXML
    // fx:id="drawingAreaAnchorPane"
    private AnchorPane drawingAreaAnchorPane; // Value injected by FXMLLoader

    @FXML
    // fx:id="mediumRadioButton"
    private RadioButton mediumRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="clearButton"
    private Button clearButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="sizeToggleGroup"
    private ToggleGroup sizeToggleGroup; // Value injected by FXMLLoader

    @FXML
    // fx:id="largeRadioButton"
    private RadioButton largeRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="greenRadioButton"
    private RadioButton greenRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="redRadioButton"
    private RadioButton redRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="blackRadioButton"
    private RadioButton blackRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="colorToggleGroup"
    private ToggleGroup colorToggleGroup; // Value injected by FXMLLoader

    @FXML
    // fx:id="blueRadioButton"
    private RadioButton blueRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="smallRadioButton"
    private RadioButton smallRadioButton; // Value injected by FXMLLoader

    @FXML
    // fx:id="undoButton"
    private Button undoButton; // Value injected by FXMLLoader

    private PenSize radius = PenSize.MEDIUM;
    private Paint brushColor = Color.BLACK;

    @FXML
    public void initialize() {
        blackRadioButton.setUserData(Color.BLACK);
        redRadioButton.setUserData(Color.RED);
        greenRadioButton.setUserData(Color.GREEN);
        blueRadioButton.setUserData(Color.BLUE);
        smallRadioButton.setUserData(PenSize.SMALL);
        mediumRadioButton.setUserData(PenSize.MEDIUM);
        largeRadioButton.setUserData(PenSize.LARGE);
    }

    @FXML
    public void drawingAreaMouseDragged(MouseEvent e) {
        Circle newCircle = new Circle(e.getX(), e.getY(), radius.getRadius(),
                brushColor);
        drawingAreaAnchorPane.getChildren().add(newCircle);
    }

    @FXML
    public void colorRadioButtonSelected(ActionEvent e) {
        brushColor = (Color) colorToggleGroup.getSelectedToggle().getUserData();
    }

    @FXML
    public void sizeRadioButtonSelected(ActionEvent e) {
        radius = (PenSize) sizeToggleGroup.getSelectedToggle().getUserData();
    }

    @FXML
    public void undoButtonPressed(ActionEvent e) {
        int count = drawingAreaAnchorPane.getChildren().size();

        if (count > 0)
            drawingAreaAnchorPane.getChildren().remove(count - 1);
    }

    @FXML
    public void clearButtonPressed(ActionEvent e) {
        drawingAreaAnchorPane.getChildren().clear();
    }

}