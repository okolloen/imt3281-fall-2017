import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class LambdaEventHandlers extends JFrame {
	
	public LambdaEventHandlers () {
		super ("Magic button");
		JButton b = new JButton ("Press me!");
		b.addActionListener((e) -> {
			System.out.println(e);
			// Note, we have access to b!!!!
			b.setText("That tickles");
		});
		add (b);
		JButton exit = new JButton ("Exit");
		// A whole less typing required
		exit.addActionListener((e)-> System.exit(0));
		add (exit, BorderLayout.SOUTH);
		pack ();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new LambdaEventHandlers();
	}

}