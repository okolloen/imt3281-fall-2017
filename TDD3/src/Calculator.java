import java.util.regex.Pattern;

public class Calculator {
	public Calculator() {
		System.out.println("Calculator opprettet");
	}

	public int add(String input) {
		int result = 0;
		if (input.length()>0) {				// We have actual input
			String delimiters = ",|\\\n";
			if (input.startsWith("//")) {	// User defined delimiter found
				String inputs[] = input.split("\\\n", 2);
				delimiters += "|"+Pattern.quote(inputs[0].substring(2));
				input = inputs[1];
			}
			String numbers[] = input.split(delimiters);	

			result = addNumbers(numbers);	// Add all numbers found
		}
		return result;
	}

	/**
	 * 
	 * @param numbers an array of strings containing the numbers to add.
	 * @return the sum of the numbers
	 * @throws NumberFormatException if any of the numbers are negative
	 */
	private int addNumbers(String numbers[]) throws NumberFormatException {
		int result = 0;
		String negativeNumbers = "";
		for (String numberAsString : numbers) {		// Go trough all numbers
			int number = Integer.parseInt(numberAsString);
			if (number<0) {							// Found negative number
				if (negativeNumbers.length()==0) {	// First negative number
					negativeNumbers = "negatives not allowed: "+number;
				} else {								// We have more than one negative number
					negativeNumbers += ", "+number;
				}
			}
			result += number;				
		}
		if (negativeNumbers.length()>0) {			// If negative numbers found
			throw new NumberFormatException(negativeNumbers);
		}
		return result;
	}
}
