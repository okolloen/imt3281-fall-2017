import static org.junit.Assert.*;

import org.hamcrest.BaseDescription;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * 
 */

/**
 * @author oeivindk
 *
 */
public class CalculatorTest {
	private static Calculator calc;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		calc = new Calculator();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		int result = calc.add("");
		assertEquals(0, result);
	}

	@Test
	public void testSingleArgument() {
		int result = calc.add("3");
		assertEquals(3, result);
		result = calc.add("8");
		assertEquals(8, result);
	}
	
	@Test
	public void testDoubleArguments() {
		int result = calc.add("1,2");
		assertEquals(3, result);
	}
	
	@Test
	public void testMultipleArguments() {
		int result = calc.add("1,2,3");
		assertEquals(6, result);
	}
	
	@Test
	public void testDelimiters() {
		int result = calc.add("1\n2,3");
		assertEquals(6, result);
	}
	
	@Test
	public void testUserDefinedDelimiter() {
		int result = calc.add("//;\n1;2\n3");
		assertEquals(6, result);
		result = calc.add("//*\n1*2*3");
		assertEquals(6, result);
	}
	
	@Test
	public void testNegativeNumbers() {
		try {
			calc.add("-1");
			fail("Exception not thrown");
		} catch (NumberFormatException nfe) {
			assertEquals("negatives not allowed: -1", nfe.getMessage());
		}
		
		try {
			calc.add("-1,3,-2");
			fail("Exception not thrown");
		} catch (NumberFormatException nfe) {
			assertEquals("negatives not allowed: -1, -2", nfe.getMessage());
		}
	}
	
}
/*public int add(String input) {
		int result = 0;
		if (input.length()>0) {
			String inputs[] = input.split(",|\\\n");
			for (String tmp : inputs) {
				result += Integer.parseInt(tmp);				
			}
		}
		return result;
	}*/





