package oppgave12_12;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class F2C extends JFrame implements ActionListener {
	JTextField farenheit = new JTextField(10);
	JLabel celcius = new JLabel();
	
	public F2C() {
		super ("Convert from Farenheit to Celcius");
		JLabel prompt = new JLabel("<html><body>Enter degrees in farenheit in<br/>the text field and press enter</body></html>");
		setLayout (new GridLayout(3, 1));
		add(prompt, BorderLayout.NORTH);
		add(farenheit, BorderLayout.CENTER);
		add(celcius, BorderLayout.SOUTH);
		farenheit.addActionListener(this);
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		double input = Double.parseDouble(e.getActionCommand());
		double celcius = 5./9*(input-32);
		this.celcius.setText(String.format("%4.2f",celcius));
	}
	
	public static void main(String[] args) {
		new F2C();
	}
}
