package oppgave12_14;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GuessTheNumber extends JFrame implements ActionListener {
	JTextField input = new JTextField(5);
	JLabel response = new JLabel();
	int number;									// The number to be guessed
	int lastDiff = 1000;							// The difference last time the user guessed
	Random randomGenerator = new Random();		// Get random generator
	JButton newGame = new JButton("Play again");
	
	public GuessTheNumber() {
		super("Welcome to Guess The Number");
		JLabel helpText = new JLabel("<html><body>I have a number between 1 and 1000. Can you guess my number?<br>Please enter your first guess.</body></html>");
		add(helpText, BorderLayout.NORTH);
		JPanel inputPanel = new JPanel();
		inputPanel.add(new JLabel("Your guess:"));
		inputPanel.add(input);
		input.addActionListener(this);
		newGame.setEnabled(false);
		JPanel io = new JPanel(new GridLayout(3, 1));
		io.add(inputPanel);
		io.add(response);
		io.add(newGame);
		newGame.addActionListener(new NewGame());
		response.setOpaque(true);
		add(io, BorderLayout.CENTER);
		number = randomGenerator.nextInt(1000)+1;	// Generate number between 1 and 1000
		pack();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	class NewGame implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {		// When user hits new game button
			newGame.setEnabled(false);
			number = randomGenerator.nextInt(1000)+1;	// Generate number between 1 and 1000
			lastDiff = 1000;
			input.setEnabled(true);
		}
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {		// When user makes a new guess
		int guess = Integer.parseInt(e.getActionCommand());
		int diff = (number-guess);
		if (diff<0)						// need the absolute value
			diff*=-1;
		if (diff<lastDiff)				// Getting warmer
			response.setBackground(Color.red);
		else								// Getting colder
			response.setBackground(Color.blue);
		lastDiff = diff;
		if (guess<number)
			response.setText("Too low");
		else if (guess>number)
			response.setText("Too high");
		else {							// User guessed the right number
			response.setText("Correct!");
			input.setEnabled(false);
			newGame.setEnabled(true);
		}
	}
	
	public static void main(String[] args) {
		new GuessTheNumber();
	}
}
