package oppgave12_10;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI extends JFrame {
	public GUI() {
		JComboBox<String> dropDown = new JComboBox<>();
		dropDown.addItem("RED");
		add(dropDown, BorderLayout.NORTH);	// Place dropdown at the top
		JPanel center = new JPanel();		// New Panels have flowlayout as default
		JCheckBox background = new JCheckBox("Background");
		center.add(background);
		JCheckBox foreground = new JCheckBox("Foreground");
		center.add(foreground);				
		add(center, BorderLayout.CENTER);	// Add panel with check boxes to center
		JPanel bottom = new JPanel();
		JButton	ok = new JButton("OK");
		bottom.add(ok);
		JButton cancel = new JButton("Cancel");
		bottom.add(cancel);
		add(bottom, BorderLayout.SOUTH);		// Add panel with buttons to the bottom
		pack();								// Make the window large enough to fit all components
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
}
