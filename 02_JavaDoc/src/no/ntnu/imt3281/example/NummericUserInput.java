package no.ntnu.imt3281.example;

import javax.swing.JOptionPane;

/**
 * Contains a single static method used to get numeric input from the user.
 * 
 * @author oivindk
 *
 */
public class NummericUserInput {
	
	/**
	 * Java isNumeric from : <a href="https://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java">
	 * https://stackoverflow.com/questions/1102891/how-to-check-if-a-string-is-numeric-in-java</a>.
	 * 
	 * @param str check this string to see if it is a number.
	 * @return true if the given string represents a number, false if does not.
	 */
	private static boolean isNumeric(String str) {
		try {	// We will cover exceptions in week 37  
			Double.parseDouble(str);  
		} catch(NumberFormatException nfe) {  
			return false;  
		}  
		return true;  
	}
	
	/**
	 * Used to get a numeric input from the user. Uses a standard 
	 * {@link javax.swing.JOptionPane#showInputDialog(java.awt.Component, Object, String, int)} to
	 * request input from the user. If input is not numeric it asks the user again.
	 * The dialog is shown as a PLAIN_MESSAGE dialog as shown in this figure.<br> 
	 * <img src="doc-files/numericInputDialog.png" alt="Screenshot of example dialog">
	 * 
	 * @param title dialog title for the dialog that pops up.
	 * @param message the label in front of the input field.
	 * @return a double with the value entered by the user or null if the user aborts the dialog.  
	 */
	public static Double getNumber(String title, String message) {
		String tmp;
		do {
			tmp = JOptionPane.showInputDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
		} while (tmp!=null && !isNumeric(tmp));
		if (tmp!=null)
			return Double.parseDouble(tmp);
		return null;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NummericUserInput []";
	}
	
	
}
