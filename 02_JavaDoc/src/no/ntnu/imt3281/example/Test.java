/**
 * 
 */
package no.ntnu.imt3281.example;

/**
 * @author oivindk
 *
 */
public class Test {

	/**
	 * @param args parameters form the command line
	 */
	public static void main(String[] args) {
		double num = NummericUserInput.getNumber("Numeric input", "Enter a number");
		System.out.println(num);
	}

}
