package no.ntnu.imt3281.laboppgave;

/**
 * Objects of this class will contain information about contacts in our contact register.
 * 
 * @author oeivindk
 *
 */
public class Contact {
	private String fname, lname, phone;
	
	/**
	 * Default constructor, creates an empty contact.
	 * 
	 */
	public Contact() {
		fname = "";
		lname = "";
		phone = "";
	}

	/**
	 * Constructor that takes contact information as parameters.
	 * 
	 * @param fname first name of contact to create.
	 * @param lname last name of contact to create.
	 * @param phone phone number for contact to create.
	 */
	public Contact(String fname, String lname, String phone) {
		this.fname = fname;
		this.lname = lname;
		this.phone = phone;
	}

	/**
	 * Get the first name for this contact.
	 * @return the first name (given name) for this contact.
	 */
	public String getFname() {
		return fname;
	}

	/**
	 * Set the first name for this contact.
	 * @param fname the first name (given name) to set as name for this contact.
	 */
	public void setFname(String fname) {
		this.fname = fname;
	}

	/**
	 * Get the last name for this contact.
	 * @return the last name (family name) for this contact.
	 */
	public String getLname() {
		return lname;
	}

	/**
	 * Set the last name for this contact.
	 * @param lname the last name (family mname) to set as name for this contact.
	 */
	public void setLname(String lname) {
		this.lname = lname;
	}

	/**
	 * Get the phone numnber for this contact.
	 * @return the phone number for this contact.
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Set the phone number for this contact.
	 * @param phone the phone number to set for this contact.
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Present information about this contact as a string.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Contact [Given name=" + fname + ", family name=" + lname + ", phone number=" + phone + "]";
	}
}
