/**
 * 
 */
package no.ntnu.imt3281.laboppgave;

import java.util.Arrays;

import javax.swing.JOptionPane;

/**
 * This class acts as a contact registry, it uses objects of class Contact to store information about up to 
 * ten different contacts.
 * 
 * @author oeivindk
 *
 */
public class ContactRegistry {
	private Contact contacts[] = new Contact[10];
	private int registeredContacts = 0;

	/**
	 * Default constructor, will ask the user to fill inn the information for all contacts
	 * using JOptionPane.inputDialog.
	 */
	public ContactRegistry() {
		String addMoreContacts = "N";
		do {
			String fname, lname, phone;
			fname = JOptionPane.showInputDialog("First name of new contact");
			if (fname!=null&&!fname.equals("")) {
				lname = JOptionPane.showInputDialog("Last name of new contact");
				if (lname!=null&&!lname.equals("")) {
					phone = JOptionPane.showInputDialog("Phone number for new contact");
					if (phone!=null&!phone.equals("")) {
						Contact contact = new Contact(fname, lname, phone);
						contacts[registeredContacts++] = contact;
					}
				}
			}
			addMoreContacts = JOptionPane.showInputDialog("Add another contact? (y/n)");
		} while (addMoreContacts.equalsIgnoreCase("y"));
	}
	
	/**
	 * Dumps the contents of the contact registry to standard output.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContactRegistry [contacts=" + Arrays.toString(contacts) + "]";
	}


	public static void main(String[] args) {
		ContactRegistry registry = new ContactRegistry();
		System.out.println(registry);
	}

}
