package no.hig.okolloen.ColorChooser;

import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class ColorChooserController {
    @FXML
    private Rectangle colorRectangle;

    @FXML
    private TextField greenTextField;

    @FXML
    private Slider greenSlider;

    @FXML
    private Slider redSlider;

    @FXML
    private TextField alphaTextField;

    @FXML
    private TextField redTextField;

    @FXML
    private Slider blueSlider;

    @FXML
    private TextField blueTextField;

    @FXML
    private Slider alphaSlider;

    private int red = 0;
    private int green = 0;
    private int blue = 0;
    private double alpha = 1.0;

    public void initialize() {
        redTextField.textProperty().bind(
                redSlider.valueProperty().asString("%.0f"));
        greenTextField.textProperty().bind(
                greenSlider.valueProperty().asString("%.0f"));
        blueTextField.textProperty().bind(
                blueSlider.valueProperty().asString("%.0f"));
        alphaTextField.textProperty().bind(
                alphaSlider.valueProperty().asString("%.2f"));

        redSlider.valueProperty().addListener(
                (observableValue, newValue, oldValue) -> {
                    red = newValue.intValue();
                    colorRectangle.setFill(Color.rgb(red, green, blue, alpha));
                });

        greenSlider.valueProperty().addListener(
                (observableValue, newValue, oldValue) -> {
                    green = newValue.intValue();
                    colorRectangle.setFill(Color.rgb(red, green, blue, alpha));
                });

        blueSlider.valueProperty().addListener(
                (observableValue, newValue, oldValue) -> {
                    blue = newValue.intValue();
                    colorRectangle.setFill(Color.rgb(red, green, blue, alpha));
                });

        alphaSlider.valueProperty().addListener(
                (observableValue, newValue, oldValue) -> {
                    alpha = newValue.doubleValue();
                    colorRectangle.setFill(Color.rgb(red, green, blue, alpha));
                });
    }

}