package no.hig.okolloen.Databases;

import java.awt.Dimension;
import java.awt.Font;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class A_SimpleMySQLSelect {

    public static void main(String[] args) {
        final String DATABASE_URL = "jdbc:mysql://kolloen.net.mysql/kolloen_net_imt3281";
        final String USERNAME = "kolloen_net_imt3281";
        final String PASSWORD = "imt3281";
        final String url = "jdbc:derby:MyDbTest";
        final String DEFAULT_QUERY = "SELECT * FROM projects";

        try {
            //java.sql.Connection con = DriverManager.getConnection(DATABASE_URL,
            //        USERNAME, PASSWORD);
        	
        		java.sql.Connection con = DriverManager.getConnection(url);
            Statement stmnt = con.createStatement();
            ResultSet res = stmnt.executeQuery(DEFAULT_QUERY);
            StringBuffer sb = new StringBuffer();
            while (res.next()) {
                sb.append(String.format("%15s %50s %20s %15s\n",
                        res.getString("id"), res.getString("repoowner"),
                        res.getString(5), res.getString(6)));
            }
            JTextArea disp = new JTextArea(sb.toString());
            disp.setFont(new Font("Monospaced", Font.PLAIN, 14));
            JScrollPane sp = new JScrollPane(disp);
            sp.setPreferredSize(new Dimension(900, 400));
            JOptionPane.showMessageDialog(null, sp, "DB result",
                    JOptionPane.PLAIN_MESSAGE);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
}
