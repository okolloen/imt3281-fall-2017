package no.hig.okolloen.Databases;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.sql.PreparedStatement;

/*
 * Databases is so much more fun when they contain data, we have a lot of contact information in the MySQL database.
 * Lets transfer it to our local database
 */
public class FillTable {
    // The URL for the local Derby database
    private final static String url = "jdbc:derby:MyDbTest";
    // This is the URL for the MySQL database
    private static final String USERNAME = "imt3281-eks";
    private static final String PASSWORD = "imt3281";
    
    public static void main(String[] args) {
        try {
            // Connect to the local Derby database
            Connection con =  DriverManager.getConnection(url);
            // We will be performing an INSERT statement over and over, create it as an prepares statement
            PreparedStatement stmt = con.prepareStatement("INSERT INTO projects "+
                    "(whatYear, projectNr, subject, repoOwner, initialDate) "+
                    "VALUES (?, ?, ?, ?, ?)");
            
            JSONArray items = null;
            
            try {
    	    			// The API at swapi.co is meant to be machine friendly. Returns data as json
    	    			JSONParser parser = new JSONParser();

    	    			// Read and parse the response
    	    			items = (JSONArray) parser.parse(new InputStreamReader(new FileInputStream("projects.json")));
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (org.json.simple.parser.ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            // Create a statement on the local database
            Statement delete = con.createStatement();
            // and delete the existing contacts
            delete.execute("DELETE FROM projects");
            
            // Keep a count of the number of contacts imported
            int counter = 0;
            
            // Loop through the result from the JSON data
            Iterator iterator = items.iterator();
        		while (iterator.hasNext()) {
        			JSONObject o = (JSONObject)iterator.next();
                // Set the values in the prepared statement
                stmt.setInt(1, Integer.parseInt((String)o.get("year")));
                stmt.setInt(2, Integer.parseInt((String)o.get("projectNr")));
                stmt.setString(3, (String)o.get("subject"));
                stmt.setString(4, (String)o.get("repoOwner"));
                stmt.setString(5, (String)o.get("initialDate"));
                // Execute the prepared statement (insert a new row in the local database)
                counter += stmt.executeUpdate();
            }
            // Close both connections
            con.close();
            
            System.out.printf("%d projects imported to DB", counter);
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
}